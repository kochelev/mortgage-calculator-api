from copy import deepcopy

# TODO: get rid of inflation attribute (for the future)
# TODO: find libs for checking equality of dictionaries


def convert_input(data):
    result = {
        "personal_data": deepcopy(data["personal_info"]),
        "credit_scheme": {
            "interest_rate": data["credit_scheme"]["interest_rate"] / 1200,
            "months": data["credit_scheme"]["months"],
        }
    }
    result['personal_data']['inflation_percent'] = 0
    mortgage_schemes = {key: convert_mortgage_scheme(mrg) for key, mrg in data["mortgage_schemes"].items()}
    result['mortgage_schemes'] = mortgage_schemes
    result['realties'] = deepcopy(data['realties'])
    return result


def convert_mortgage_scheme(mrg):
    mrg["schedule"] = [{
        "interest_rate": period["interest_rate"] / 1200,
        "months": period["months"]}
        for period in mrg["schedule"]]
    return mrg


def convert_realty(rlt):
    return {
        "id": rlt["id"],
        "title": rlt["title"],
        "link": rlt["region"],
        "cost": rlt["cost"],
        "is_primary": rlt["is_primary"],
        "get_keys_month": rlt["gotkeys_month"],
        "repairing_expenses": rlt["repairing"]["expencies"],
        "repairing_months": rlt["repairing"]["months"],
        "area": rlt["area"],
        "subway_distance": rlt["subway_distance"],
        "has_mall": rlt["has_mall"],
    }


def convert_output(output_data):
    ui_precision = 2

    def handle_case(case):
        case['success'] = True
        case['deal_month'] = case['x'][0]
        case['repairing_gap'] = case['x'][1]
        del case['x']
        case.update(case['result'])
        del case['result']
        case['savings'] = round(case['savings'], ui_precision)
        return case
    
    for realty in output_data.values():
        for mortgage in realty.values():
            del mortgage['combs']
            if mortgage['opt'] is None:
                mortgage['savings'] = {'success': False}
                mortgage['credit'] = {'success': False}
            else:
                if 'savings' in mortgage['opt']:
                    mortgage['savings'] = mortgage['opt']['savings']
                    mortgage['savings'] = handle_case(mortgage['savings'])
                else:
                    mortgage['savings'] = {'success': False}
                if 'credit' in mortgage['opt']:
                    mortgage['credit'] = mortgage['opt']['credit']
                    mortgage['credit'] = handle_case(mortgage['credit'])
                else:
                    mortgage['credit'] = {'success': False}

            del mortgage['opt']

    return output_data


def clarify_input(data):

    matches = {
        'None': None,
        'none': None,
        'null': None,
        'True': True,
        'true': True,
        'False': False,
        'false': False
    }

    if isinstance(data, list):
        temp_list = []
        for element in data:
            temp_list.append(clarify_input(element))
        return temp_list
    elif isinstance(data, dict):
        temp_dict = {}
        for key, value in data.items():
            temp_dict[key] = clarify_input(value)
        return temp_dict
    else:
        if data in matches:
            return matches[data]
        return data


def clarify_output(data):

    if isinstance(data, dict):
        new_data = {}
        for key, value in data.items():
            new_value = clarify_output(value)
            if isinstance(key, int) or isinstance(key, tuple):
                new_data[str(key)] = new_value
            else:
                new_data[key] = new_value
        return new_data
    elif isinstance(data, list):
        return [clarify_output(e) for e in data]
    else:
        return data


def is_equal(x_1, x_2, float_prc):

    def func(x, y):
        
        if type(x) != type(y):
            return False
        
        if x is None:
            return True
        
        if isinstance(x, int) or isinstance(x, bool) or isinstance(x, str):
            if x != y:
                return False
            else:
                return True
        
        if isinstance(x, float):
            if round(x, float_prc) != round(y, float_prc):
                return False
            else:
                return True

        if isinstance(x, list):
            if len(x) != len(y):
                return False
            for e in range(len(x)):
                if not func(x[e], y[e]):
                    return False
            return True
        
        if isinstance(x, dict):
            if len(x) != len(y):
                return False
            for x_key, x_value in x.items():
                if x_key not in y:
                    return False
                if not func(x_value, y[x_key]):
                    return False
            return True

    return func(x_1, x_2)
