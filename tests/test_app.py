import app as tested_app
import unittest
import json
from app import convert_input, convert_output, compare
from test_scheduler_cases import cases

# TODO: make this tests as an API tests


class TestPing(unittest.TestCase):

    def setUp(self):
        tested_app.app.testing = True
        self.app = tested_app.app.test_client()

    # Endpoint: /

    def test_ping(self):
        expected = b'Hello World! I\'m Mortgage Calculator ++'
        result = self.app.get('/')
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.data, expected)


# class TestCompare(unittest.TestCase):
#
#     def setUp(self):
#         tested_app.app.testing = True
#         self.app = tested_app.app.test_client()
#
#     def test_positive(self):
#         result = self.app.post('/compare',
#                                content_type='application/json',
#                                data=json.dumps({'age': 25, 'number_of_credits': 2}))
#         self.assertEqual(r.json, {'score': 50})
#         self.assertEqual(r.status_code, 200)
#
#     def test_post_hello_endpoint(self):
#         r = self.app.post('/')
#         self.assertEqual(r.status_code, 405)
#
#     # Endpoint: /api/v1.0/score
#
#     def test_get_api_endpoint(self):
#         r = self.app.get('/api/v1.0/score')
#         self.assertEqual(r.status_code, 405)
#
#     def test_home(self):
#         expected = b'Hello World! I\'m Mortgage Calculator ++'
#         result = self.app.get('/')
#         print('*' * 100)
#         print(result)
#         self.assertEqual(result, expected)
#         Make your assertions
#
# class TestCompare(unittest.TestCase):
#     pass
#     def test(self):
#         expected = [{'id': 891747736, 'title': 'Primary 3', 'link': 'Paris', 'cost': 6000000, 'is_primary': True, 'get_keys_month': 29, 'repairing_expenses': 1200000, 'repairing_months': 3, 'area': 34, 'subway_distance': 15, 'has_mall': True, 'schemes': {'no_mortgage': {}, 245242248: {'savings': {'deal_month': 11, 'initial_debt': 0, 'initial_payment': 0, 'total_months': 88, 'years': 7.33, 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 1200000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': None}, 'times': 11}, {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 4493452.2, 'schedule': [{'interest_rate': 0.004, 'months': 20}, {'interest_rate': 0.01, 'months': 280}], 'minimal_initial_sum': 600000.0, 'expencies': 60000}}, {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'buy_realty': 6000000}}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 44252.655}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 44252.655404381556}, 'times': 2}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 44252.65540438155}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 44252.65540438154}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 44252.655404381556}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 44252.65540438155}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 44252.655404381556}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 44252.65540438155}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 44252.655404381556}, 'times': 2}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 44252.65540438156}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 44252.655404381556}, 'times': 2}, {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'repairing': 1200000}}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': None}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': None, 'to_save': None}, 'times': 56}]}}, 563494521: {'savings': {'deal_month': 11, 'initial_debt': 0, 'initial_payment': 0, 'total_months': 81, 'years': 6.75, 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 1200000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': None}, 'times': 11}, {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 4632174.17, 'schedule': [{'interest_rate': 0.005083333333333333, 'months': 240}], 'minimal_initial_sum': 900000.0, 'expencies': 60000}}, {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'buy_realty': 6000000}}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 36545.879}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 36545.879281354566}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 36545.87928135457}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 36545.879281354566}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 36545.87928135457}, 'times': 6}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 36545.879281354566}, 'times': 2}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 36545.87928135457}, 'times': 2}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 36545.879281354566}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 36545.87928135457}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 36545.87928135458}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 36545.87928135457}}, {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'repairing': 1200000}}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': None}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': None, 'to_save': None}, 'times': 49}]}}}}, {'id': 949895199, 'title': 'Primary 1', 'link': '', 'cost': 6000000, 'is_primary': True, 'get_keys_month': 7, 'repairing_expenses': 300000, 'repairing_months': 3, 'area': None, 'subway_distance': None, 'has_mall': False, 'schemes': {'no_mortgage': {}, 245242248: {'savings': {'deal_month': 6, 'initial_debt': 0, 'initial_payment': 0, 'total_months': 65, 'years': 5.42, 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 1200000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': None}, 'times': 6}, {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 4696913.169773721, 'schedule': [{'interest_rate': 0.004, 'months': 20}, {'interest_rate': 0.01, 'months': 280}], 'minimal_initial_sum': 600000.0, 'expencies': 60000}}, {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'buy_realty': 6000000}}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 43086.8302262785}}, {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'repairing': 300000}}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': None}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': None, 'to_save': None}, 'times': 55}]}, 'credit': {'credit_period': 0, 'deal_month': 7, 'initial_debt': 0, 'initial_payment': 0, 'total_months': 65, 'years': 5.42, 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 1200000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': None}, 'times': 7}, {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 4370000.0, 'schedule': [{'interest_rate': 0.004, 'months': 20}, {'interest_rate': 0.01, 'months': 280}], 'minimal_initial_sum': 600000.0, 'expencies': 60000}}, {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'buy_realty': 6000000}}}, {'cmd': 'get_credit', 'payload': {'name': 'Repairing', 'requested_sum': 300000, 'schedule': [{'interest_rate': 0.015, 'months': 24}], 'minimal_initial_sum': 0, 'expencies': 0}}, {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'repairing': 300000}}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': None}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': None, 'to_save': None}, 'times': 55}]}}, 563494521: {'savings': {'deal_month': 6, 'initial_debt': 0, 'initial_payment': 0, 'total_months': 62, 'years': 5.17, 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 1200000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': None}, 'times': 6}, {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 4703972.658063845, 'schedule': [{'interest_rate': 0.005083333333333333, 'months': 240}], 'minimal_initial_sum': 900000.0, 'expencies': 60000}}, {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'buy_realty': 6000000}}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': 36027.34193615494}}, {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'repairing': 300000}}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': None}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': None, 'to_save': None}, 'times': 52}]}, 'credit': {'credit_period': 0, 'deal_month': 7, 'initial_debt': 0, 'initial_payment': 0, 'total_months': 62, 'years': 5.17, 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 1200000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': None}, 'times': 7}, {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 4370000.0, 'schedule': [{'interest_rate': 0.005083333333333333, 'months': 240}], 'minimal_initial_sum': 900000.0, 'expencies': 60000}}, {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'buy_realty': 6000000}}}, {'cmd': 'get_credit', 'payload': {'name': 'Repairing', 'requested_sum': 300000, 'schedule': [{'interest_rate': 0.015, 'months': 24}], 'minimal_initial_sum': 0, 'expencies': 0}}, {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'repairing': 300000}}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 30000}, 'to_save': None}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': None, 'to_save': None}, 'times': 52}]}}}}]
#         with open('test_app_input_data.json', 'r') as json_file:
#             data = json.load(json_file)
#             result = compare(data)
#             print('*' * 100)
#             print(result)
#             self.assertEqual(result, expected, 'Should be %s' % str(expected))
#
# class TestExpand(unittest.TestCase):
#     pass
