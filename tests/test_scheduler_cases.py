cases = {
    "ex__CantStart__x_1__0": {
        "input_data": {
            "personal_data": {
                "current_savings": 300000,
                "month_income": 100000,
                "month_rent": 50000,
                "month_expenses": 0,
                "deal_month_start": 0,
                "deal_month_finish": 12,
                "max_repairing_delay_months": 12,
                "inflation_percent": 0,
            },
            "credit_scheme": {
                "interest_rate": 3.2 / 1200,
                "months": 12,
            },
            "mortgage_schemes": {
                32342390239: {
                    "id": 32342390239,
                    "title": "12345678901234567890",
                    "initial_payment_percent": 15,
                    "initial_expenses": 60000,
                    "schedule": [
                        {
                            "interest_rate": 6.5 / 1200,
                            "months": 12,
                        },
                        {
                            "interest_rate": 10 / 1200,
                            "months": 228,
                        }
                    ]
                }
            },
            "realties": {
                83927978940983: {
                    "id": 83927978940983,
                    "title": "12345678901234567890",
                    "link": "https://yandex.ru",
                    "cost": 1800000,
                    "is_primary": True,
                    "get_keys_month": 14,
                    "repairing_expenses": 1000000,
                    "repairing_months": 3,
                    "area": None,
                    "subway_distance": None,
                    "has_mall": None,
                }
            }
        },
        "output_data": {83927978940983: {'no_mortgage': {'opt': None, 'combs': {(0, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}}, (0, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}}, (0, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}}, (0, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}}, (0, 4): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}}, (0, 5): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}}, (0, 6): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}}, (0, 7): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}}, (0, 8): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}}, (0, 9): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}}, (0, 10): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}}, (0, 11): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 300000, 'need': 1800000}}}, (1, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}}, (1, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}}, (1, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}}, (1, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}}, (1, 4): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}}, (1, 5): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}}, (1, 6): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}}, (1, 7): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}}, (1, 8): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}}, (1, 9): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}}, (1, 10): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}}, (1, 11): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 350000, 'need': 1800000}}}, (2, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}}, (2, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}}, (2, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}}, (2, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}}, (2, 4): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}}, (2, 5): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}}, (2, 6): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}}, (2, 7): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}}, (2, 8): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}}, (2, 9): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}}, (2, 10): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}}, (2, 11): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 400000, 'need': 1800000}}}, (3, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}}, (3, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}}, (3, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}}, (3, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}}, (3, 4): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}}, (3, 5): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}}, (3, 6): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}}, (3, 7): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}}, (3, 8): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}}, (3, 9): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}}, (3, 10): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}}, (3, 11): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 450000, 'need': 1800000}}}, (4, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}}, (4, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}}, (4, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}}, (4, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}}, (4, 4): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}}, (4, 5): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}}, (4, 6): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}}, (4, 7): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}}, (4, 8): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}}, (4, 9): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}}, (4, 10): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}}, (4, 11): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 500000, 'need': 1800000}}}, (5, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}}, (5, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}}, (5, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}}, (5, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}}, (5, 4): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}}, (5, 5): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}}, (5, 6): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}}, (5, 7): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}}, (5, 8): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}}, (5, 9): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}}, (5, 10): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}}, (5, 11): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 550000, 'need': 1800000}}}, (6, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}}, (6, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}}, (6, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}}, (6, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}}, (6, 4): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}}, (6, 5): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}}, (6, 6): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}}, (6, 7): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}}, (6, 8): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}}, (6, 9): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}}, (6, 10): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}}, (6, 11): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 600000, 'need': 1800000}}}, (7, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}}, (7, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}}, (7, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}}, (7, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}}, (7, 4): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}}, (7, 5): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}}, (7, 6): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}}, (7, 7): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}}, (7, 8): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}}, (7, 9): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}}, (7, 10): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}}, (7, 11): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 650000, 'need': 1800000}}}, (8, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}}, (8, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}}, (8, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}}, (8, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}}, (8, 4): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}}, (8, 5): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}}, (8, 6): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}}, (8, 7): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}}, (8, 8): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}}, (8, 9): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}}, (8, 10): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}}, (8, 11): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 700000, 'need': 1800000}}}, (9, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}}, (9, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}}, (9, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}}, (9, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}}, (9, 4): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}}, (9, 5): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}}, (9, 6): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}}, (9, 7): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}}, (9, 8): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}}, (9, 9): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}}, (9, 10): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}}, (9, 11): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 750000, 'need': 1800000}}}, (10, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}}, (10, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}}, (10, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}}, (10, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}}, (10, 4): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}}, (10, 5): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}}, (10, 6): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}}, (10, 7): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}}, (10, 8): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}}, (10, 9): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}}, (10, 10): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}}, (10, 11): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 800000, 'need': 1800000}}}, (11, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}}, (11, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}}, (11, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}}, (11, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}}, (11, 4): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}}, (11, 5): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}}, (11, 6): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}}, (11, 7): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}}, (11, 8): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}}, (11, 9): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}}, (11, 10): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}}, (11, 11): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 850000, 'need': 1800000}}}}}, 32342390239: {'opt': {'savings': {'case': 'S_i < C, S < S_t', 'x': (11, 10), 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 300000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 11}, {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 1509490.8900000001, 'schedule': [{'interest_rate': 0.005416666666666667, 'months': 12}, {'interest_rate': 0.008333333333333333, 'months': 228}], 'minimal_initial_sum': 270000.0, 'expenses': 60000}}, {'cmd': 'spend_savings', 'payload': {'buy_realty': 1800000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000, 'to_save': 38745.641}, 'times': 12}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000, 'to_save': 35561.413}}, {'cmd': 'spend_savings', 'payload': {'repairing': 1000000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000}, 'times': 15}], 'result': {'months': 42, 'savings': 56070.04154486792, 'criteria': 41.43929958455132}}}, 'combs': {0: {'savings': {'success': False, 'code': 'S_i < IP_min + R', 'payload': {'need': 330000.0, 'have': 300000}, 'message': 'Too big IP: need 330000 but have 300000'}, 'credit': {'success': False, 'code': 'S_i < IP_min + R', 'payload': {'need': 330000.0, 'have': 300000}, 'message': 'Too big IP: need 330000 but have 300000'}}, (1, 0): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 94132.39385200507, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (1, 1): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 93732.29958329163, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (1, 2): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 93327.65725511464, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (1, 3): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 92918.40304762272, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (1, 4): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 92504.47199914091, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (1, 5): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 92085.79798054796, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (1, 6): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91662.31366895832, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (1, 7): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91233.95052068675, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (1, 8): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90800.63874347227, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (1, 9): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90362.3072679382, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (1, 10): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89918.8837182632, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (1, 11): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89470.29438203784, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (2, 0): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 94004.51494406577, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (2, 1): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 93603.76165108742, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (2, 2): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 93198.46467537887, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (2, 3): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 92788.56049859281, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (2, 4): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 92373.98447098349, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (2, 5): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91954.67078613362, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (2, 6): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91530.5524549985, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (2, 7): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91101.5612792458, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (2, 8): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90667.62782386853, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (2, 9): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90228.68138904792, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (2, 10): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89784.64998124223, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (2, 11): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89335.4602834764, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (3, 0): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91891.27624743292, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (3, 1): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 93481.02712390013, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (3, 2): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 93075.12945016976, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (3, 3): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 92664.62976746609, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (3, 4): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 92249.46374207098, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (3, 5): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91829.56589434424, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (3, 6): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91404.86957312631, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (3, 7): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90975.30692944903, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (3, 8): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90540.80888953328, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (3, 9): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90101.30512705032, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (3, 10): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89656.72403462358, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (3, 11): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89206.99269454624, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (4, 0): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91814.2685125025, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (4, 1): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91486.33358043789, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (4, 2): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 92957.5393037345, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (4, 3): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 92546.49724925209, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (4, 4): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 92130.79485955332, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (4, 5): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91710.36698554913, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (4, 6): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91285.147317705, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (4, 7): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90855.06836011901, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (4, 8): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90420.06140389957, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (4, 9): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89980.05649982076, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (4, 10): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89534.9824302321, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (4, 11): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89084.76668019923, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (5, 0): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91740.17112087063, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (5, 1): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91412.39280757346, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (5, 2): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91081.39091344285, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (5, 3): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 92434.05148356888, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (5, 4): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 92017.86504833444, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (5, 5): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91596.95995164056, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (5, 6): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91171.2702290273, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (5, 7): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90740.72874106577, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (5, 8): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90305.26714711172, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (5, 9): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89864.81587835011, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (5, 10): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89419.30411010822, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (5, 11): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 88968.6597334139, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (6, 0): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91668.93700647565, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (6, 1): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91341.33250929591, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (6, 2): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91010.5171026444, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (6, 3): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90676.44824644782, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (6, 4): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91910.56366340326, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (6, 5): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91489.23284741677, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (6, 6): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91063.12504372778, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (6, 7): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90632.17347250547, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (6, 8): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90196.31016442654, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (6, 9): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89755.46593410443, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (6, 10): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89309.57035280066, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (6, 11): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 88858.5517203957, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (7, 0): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91600.51992973905, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (7, 1): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91273.1060100882, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (7, 2): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90942.49389772119, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (7, 3): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90608.64139771536, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (7, 4): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90271.50557945279, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (7, 5): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91387.07584323762, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (7, 6): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90960.60064649911, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (7, 7): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90529.29013581501, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (7, 8): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90093.07671598364, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (7, 9): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89651.89158778731, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (7, 10): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89205.66472109713, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (7, 11): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 88754.3248272512, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (8, 0): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91534.87445995666, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (8, 1): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91207.66745422376, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (8, 2): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90877.27501370077, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (8, 3): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90543.65528614646, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (8, 4): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90206.76569278631, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (8, 5): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89866.56291245775, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (8, 6): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90863.58802307199, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (8, 7): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90431.96844558146, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (8, 8): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89995.45522790225, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (8, 9): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89553.97995946181, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (8, 10): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89107.47301114807, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (8, 11): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 88655.86350808984, 'have': 50000}}, 'savings': {'success': True, 'months': 43, 'savings': 56294.727431492334, 'criteria': 42.43705272568508}}, (9, 0): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91471.955958139, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (9, 1): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91144.9717883005, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (9, 2): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90814.81497870847, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (9, 3): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90481.44401731333, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (9, 4): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90144.81667457175, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (9, 5): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89804.88998785726, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (9, 6): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89461.62024546272, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (9, 7): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90340.10020290637, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (9, 8): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89903.33624466379, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (9, 9): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89461.62031998948, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (9, 10): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89014.88320293996, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (9, 11): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 88563.05443450884, 'have': 50000}}, 'savings': {'success': True, 'months': 43, 'savings': 72400.4014851217, 'criteria': 42.27599598514878}}, (10, 0): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91411.72056028755, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (10, 1): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91084.97474422067, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (10, 2): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90755.06911664437, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (10, 3): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90421.96250319318, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (10, 4): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90085.6130209259, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (10, 5): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89745.97806299702, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (10, 6): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89403.01428292821, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (10, 7): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89056.67757846769, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (10, 8): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89816.61238274073, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (10, 9): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89374.70404374613, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (10, 10): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 88927.7854120767, 'have': 50000}}, 'savings': {'success': True, 'months': 42, 'savings': 40339.752627669666, 'criteria': 41.5966024737233}}, (10, 11): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 88475.78644641813, 'have': 50000}}, 'savings': {'success': True, 'months': 43, 'savings': 88177.93213845948, 'criteria': 42.11822067861541}}, (11, 0): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91354.12516109375, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (11, 1): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 91027.63282260257, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (11, 2): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90697.99353030232, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (11, 3): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90365.16644498824, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (11, 4): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 90029.11002767789, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (11, 5): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89689.78202453846, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (11, 6): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89347.1394514223, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (11, 7): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89001.13857799915, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (11, 8): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 88651.73491147265, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (11, 9): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 89293.1245625751, 'have': 50000}}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (11, 10): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 88846.07184282847, 'have': 50000}}, 'savings': {'success': True, 'months': 42, 'savings': 56070.04154486792, 'criteria': 41.43929958455132}}, (11, 11): {'credit': {'success': False, 'code': 'MRG_MP + CRD_MP', 'payload': {'need': 88393.95050416393, 'have': 50000}}, 'savings': {'success': True, 'months': 42, 'savings': 3607.6418169433455, 'criteria': 41.963923581830564}}}}}},
        "expanded": {
            83927978940983: {
                32342390239: {
                    'savings': [
                        {
                            "after_savings": 300000,
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 300000,
                                "irregular_income": 0,
                                "regular_income": 0
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": 350000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 300000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 400000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 350000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 450000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 400000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 500000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 450000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 550000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 500000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 600000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 550000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 650000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 600000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 700000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 650000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 750000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 700000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 800000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 750000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 499490.89000000013,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 0,
                                    "after_closing_payment": 1517667.2989875001,
                                    "after_debt": 1509490.8900000001,
                                    "after_minimal_payment": 11254.358558887789,
                                    "before_closing_payment": 0,
                                    "before_debt": 0,
                                    "before_minimal_payment": 0,
                                    "interest_rate": 0.005416666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 1910000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 800000,
                                "irregular_income": 1509490.8900000001,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 538236.5310000002,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 11254.358999999995,
                                    "after_closing_payment": 1514572.6767457658,
                                    "after_debt": 1506412.9399875002,
                                    "after_minimal_payment": 11254.358555592255,
                                    "before_closing_payment": 1517667.2989875001,
                                    "before_debt": 1509490.8900000001,
                                    "before_minimal_payment": 11254.358558887789,
                                    "interest_rate": 0.005416666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 499490.89000000013,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 38745.641
                        },
                        {
                            "after_savings": 576982.1720000003,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 11254.358999999997,
                                    "after_closing_payment": 1511461.2919668888,
                                    "after_debt": 1503318.317745766,
                                    "after_minimal_payment": 11254.358552265265,
                                    "before_closing_payment": 1514572.6767457658,
                                    "before_debt": 1506412.9399875002,
                                    "before_minimal_payment": 11254.358555592255,
                                    "interest_rate": 0.005416666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 538236.5310000002,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 38745.641
                        },
                        {
                            "after_savings": 615727.8130000003,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 11254.358999999997,
                                    "after_closing_payment": 1508333.0538537928,
                                    "after_debt": 1500206.9329668889,
                                    "after_minimal_payment": 11254.358548906419,
                                    "before_closing_payment": 1511461.2919668888,
                                    "before_debt": 1503318.317745766,
                                    "before_minimal_payment": 11254.358552265265,
                                    "interest_rate": 0.005416666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 576982.1720000003,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 38745.641
                        },
                        {
                            "after_savings": 654473.4540000004,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 11254.358999999995,
                                    "after_closing_payment": 1505187.8711175842,
                                    "after_debt": 1497078.694853793,
                                    "after_minimal_payment": 11254.358545515302,
                                    "before_closing_payment": 1508333.0538537928,
                                    "before_debt": 1500206.9329668889,
                                    "before_minimal_payment": 11254.358548906419,
                                    "interest_rate": 0.005416666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 615727.8130000003,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 38745.641
                        },
                        {
                            "after_savings": 693219.0950000004,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 11254.358999999999,
                                    "after_closing_payment": 1502025.6519748878,
                                    "after_debt": 1493933.5121175842,
                                    "after_minimal_payment": 11254.358542091497,
                                    "before_closing_payment": 1505187.8711175842,
                                    "before_debt": 1497078.694853793,
                                    "before_minimal_payment": 11254.358545515302,
                                    "interest_rate": 0.005416666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 654473.4540000004,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 38745.641
                        },
                        {
                            "after_savings": 731964.7360000005,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 11254.358999999997,
                                    "after_closing_payment": 1498846.3041451685,
                                    "after_debt": 1490771.2929748879,
                                    "after_minimal_payment": 11254.358538634584,
                                    "before_closing_payment": 1502025.6519748878,
                                    "before_debt": 1493933.5121175842,
                                    "before_minimal_payment": 11254.358542091497,
                                    "interest_rate": 0.005416666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 693219.0950000004,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 38745.641
                        },
                        {
                            "after_savings": 770710.3770000006,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 11254.359,
                                    "after_closing_payment": 1495649.7348480383,
                                    "after_debt": 1487591.9451451686,
                                    "after_minimal_payment": 11254.35853514413,
                                    "before_closing_payment": 1498846.3041451685,
                                    "before_debt": 1490771.2929748879,
                                    "before_minimal_payment": 11254.358538634584,
                                    "interest_rate": 0.005416666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 731964.7360000005,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 38745.641
                        },
                        {
                            "after_savings": 809456.0180000006,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 11254.358999999999,
                                    "after_closing_payment": 1492435.8508005482,
                                    "after_debt": 1484395.3758480381,
                                    "after_minimal_payment": 11254.358531619695,
                                    "before_closing_payment": 1495649.7348480383,
                                    "before_debt": 1487591.9451451686,
                                    "before_minimal_payment": 11254.35853514413,
                                    "interest_rate": 0.005416666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 770710.3770000006,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 38745.641
                        },
                        {
                            "after_savings": 848201.6590000007,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 11254.358999999993,
                                    "after_closing_payment": 1489204.558214468,
                                    "after_debt": 1481181.4918005483,
                                    "after_minimal_payment": 11254.358528060833,
                                    "before_closing_payment": 1492435.8508005482,
                                    "before_debt": 1484395.3758480381,
                                    "before_minimal_payment": 11254.358531619695,
                                    "interest_rate": 0.005416666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 809456.0180000006,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 38745.641
                        },
                        {
                            "after_savings": 886947.3000000007,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 11254.358999999999,
                                    "after_closing_payment": 1485955.7627935463,
                                    "after_debt": 1477950.199214468,
                                    "after_minimal_payment": 11254.358524467092,
                                    "before_closing_payment": 1489204.558214468,
                                    "before_debt": 1481181.4918005483,
                                    "before_minimal_payment": 11254.358528060833,
                                    "interest_rate": 0.005416666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 848201.6590000007,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 38745.641
                        },
                        {
                            "after_savings": 925692.9410000008,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 11254.358999999999,
                                    "after_closing_payment": 1482689.3697307615,
                                    "after_debt": 1474701.4037935464,
                                    "after_minimal_payment": 11254.358520838006,
                                    "before_closing_payment": 1485955.7627935463,
                                    "before_debt": 1477950.199214468,
                                    "before_minimal_payment": 11254.358524467092,
                                    "interest_rate": 0.005416666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 886947.3000000007,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 38745.641
                        },
                        {
                            "after_savings": 964438.5820000009,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 11254.358999999993,
                                    "after_closing_payment": 1483696.9691535176,
                                    "after_debt": 1471435.0107307613,
                                    "after_minimal_payment": 14438.587200536516,
                                    "before_closing_payment": 1482689.3697307615,
                                    "before_debt": 1474701.4037935464,
                                    "before_minimal_payment": 11254.358520838006,
                                    "interest_rate": 0.005416666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 925692.9410000008,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 38745.641
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 14438.587,
                                    "after_closing_payment": 1481502.2020047968,
                                    "after_debt": 1469258.3821535176,
                                    "after_minimal_payment": 14438.587202507211,
                                    "before_closing_payment": 1483696.9691535176,
                                    "before_debt": 1471435.0107307613,
                                    "before_minimal_payment": 14438.587200536516,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 1050000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 964438.5820000009,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 35561.413
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 50000.0,
                                    "after_closing_payment": 1443431.3870215036,
                                    "after_debt": 1431502.202004797,
                                    "after_minimal_payment": 14088.597907877918,
                                    "before_closing_payment": 1481502.2020047968,
                                    "before_debt": 1469258.3821535176,
                                    "before_minimal_payment": 14438.587202507211,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 50000.0,
                                    "after_closing_payment": 1405043.315246683,
                                    "after_debt": 1393431.3870215039,
                                    "after_minimal_payment": 13734.63012055814,
                                    "before_closing_payment": 1443431.3870215036,
                                    "before_debt": 1431502.202004797,
                                    "before_minimal_payment": 14088.597907877918,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 50000.0,
                                    "after_closing_payment": 1366335.342873739,
                                    "after_debt": 1355043.3152466833,
                                    "after_minimal_payment": 13376.628013042935,
                                    "before_closing_payment": 1405043.315246683,
                                    "before_debt": 1393431.3870215039,
                                    "before_minimal_payment": 13734.63012055814,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 1276888.1373976874,
                                    "after_debt": 1266335.3428737395,
                                    "after_minimal_payment": 12520.187523338658,
                                    "before_closing_payment": 1366335.342873739,
                                    "before_debt": 1355043.3152466833,
                                    "before_minimal_payment": 13376.628013042935,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 1186695.5385426686,
                                    "after_debt": 1176888.1373976879,
                                    "after_minimal_payment": 11653.933631959067,
                                    "before_closing_payment": 1276888.1373976874,
                                    "before_debt": 1266335.3428737395,
                                    "before_minimal_payment": 12520.187523338658,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 1095751.3346971911,
                                    "after_debt": 1086695.5385426688,
                                    "after_minimal_payment": 10777.72704506461,
                                    "before_closing_payment": 1186695.5385426686,
                                    "before_debt": 1176888.1373976879,
                                    "before_minimal_payment": 11653.933631959067,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 1004049.2624863345,
                                    "after_debt": 995751.3346971913,
                                    "after_minimal_payment": 9891.425942930462,
                                    "before_closing_payment": 1095751.3346971911,
                                    "before_debt": 1086695.5385426688,
                                    "before_minimal_payment": 10777.72704506461,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 911583.0063403876,
                                    "after_debt": 904049.2624863349,
                                    "after_minimal_payment": 8994.885922477028,
                                    "before_closing_payment": 1004049.2624863345,
                                    "before_debt": 995751.3346971913,
                                    "before_minimal_payment": 9891.425942930462,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 818346.1980598912,
                                    "after_debt": 811583.006340388,
                                    "after_minimal_payment": 8087.959938219323,
                                    "before_closing_payment": 911583.0063403876,
                                    "before_debt": 904049.2624863349,
                                    "before_minimal_payment": 8994.885922477028,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 724332.4163770573,
                                    "after_debt": 718346.1980598916,
                                    "after_minimal_payment": 7170.498241584209,
                                    "before_closing_payment": 818346.1980598912,
                                    "before_debt": 811583.006340388,
                                    "before_minimal_payment": 8087.959938219323,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 629535.1865135331,
                                    "after_debt": 624332.4163770577,
                                    "after_minimal_payment": 6242.3483185425375,
                                    "before_closing_payment": 724332.4163770573,
                                    "before_debt": 718346.1980598916,
                                    "before_minimal_payment": 7170.498241584209,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 533947.9797344796,
                                    "after_debt": 529535.1865135335,
                                    "after_minimal_payment": 5303.354825501397,
                                    "before_closing_payment": 629535.1865135331,
                                    "before_debt": 624332.4163770577,
                                    "before_minimal_payment": 6242.3483185425375,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 437564.2128989339,
                                    "after_debt": 433947.9797344799,
                                    "after_minimal_payment": 4353.359523399461,
                                    "before_closing_payment": 533947.9797344796,
                                    "before_debt": 529535.1865135335,
                                    "before_minimal_payment": 5303.354825501397,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 340377.2480064254,
                                    "after_debt": 337564.21289893426,
                                    "after_minimal_payment": 3392.201209946442,
                                    "before_closing_payment": 437564.2128989339,
                                    "before_debt": 433947.9797344799,
                                    "before_minimal_payment": 4353.359523399461,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 242380.39173981262,
                                    "after_debt": 240377.24800642574,
                                    "after_minimal_payment": 2419.7156499453117,
                                    "before_closing_payment": 340377.2480064254,
                                    "before_debt": 337564.21289893426,
                                    "before_minimal_payment": 3392.201209946442,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 143566.8950043114,
                                    "after_debt": 142380.39173981297,
                                    "after_minimal_payment": 1435.7355036337035,
                                    "before_closing_payment": 242380.39173981262,
                                    "before_debt": 240377.24800642574,
                                    "before_minimal_payment": 2419.7156499453117,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": -0.00499999918974936,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 43929.952462681016,
                                    "after_debt": 43566.89500431175,
                                    "after_minimal_payment": 440.0902529784397,
                                    "before_closing_payment": 143566.8950043114,
                                    "before_debt": 142380.39173981297,
                                    "before_minimal_payment": 1435.7355036337035,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": 56070.042537319794,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 43929.952462681016,
                                    "after_closing_payment": 0,
                                    "after_debt": 0,
                                    "after_minimal_payment": 0,
                                    "before_closing_payment": 43929.952462681016,
                                    "before_debt": 43566.89500431175,
                                    "before_minimal_payment": 440.0902529784397,
                                    "interest_rate": 0.008333333333333333
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": -0.00499999918974936,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 56070.047537318984,
                            "to_save": 0
                        }
                    ]
                }
            }
        }
    },
    "ex__CantStart__x_1__0_2": {
        "input_data": {
            "personal_data": {
                "current_savings": 300000,
                "month_income": 100000,
                "month_rent": 50000,
                "month_expenses": 0,
                "deal_month_start": 28,
                "deal_month_finish": 35,
                "max_repairing_delay_months": 12,
                "inflation_percent": 0,
            },
            "credit_scheme": {
                "interest_rate": 3.2 / 1200,
                "months": 12,
            },
            "mortgage_schemes": {
                32342390239: {
                    "id": 32342390239,
                    "title": "12345678901234567890",
                    "initial_payment_percent": 15,
                    "initial_expenses": 60000,
                    "schedule": [
                        {
                            "interest_rate": 6.5 / 1200,
                            "months": 12,
                        },
                        {
                            "interest_rate": 10 / 1200,
                            "months": 228,
                        }
                    ]
                }
            },
            "realties": {
                83927978940983: {
                    "id": 83927978940983,
                    "title": "12345678901234567890",
                    "link": "https://yandex.ru",
                    "cost": 1800000,
                    "is_primary": True,
                    "get_keys_month": 44,
                    "repairing_expenses": 1000000,
                    "repairing_months": 3,
                    "area": None,
                    "subway_distance": None,
                    "has_mall": None,
                }
            }
        },
        "output_data": {83927978940983: {'no_mortgage': {'opt': {'credit': {'case': 'S_i > C, S < S_t', 'x': (30, 0), 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 300000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 30}, {'cmd': 'spend_savings', 'payload': {'buy_realty': 1800000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 14}, {'cmd': 'get_credit', 'payload': {'name': 'Repairing', 'requested_sum': 300000, 'schedule': [{'interest_rate': 0.002666666666666667, 'months': 12}], 'minimal_initial_sum': 0, 'expenses': 0}}, {'cmd': 'spend_savings', 'payload': {'repairing': 1000000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000}, 'times': 2}], 'result': {'months': 49, 'savings': 47448.836667370124, 'criteria': 48.525511633326296}}, 'savings': {'case': 'S_i > C, S > S_t', 'x': (30, 6), 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 300000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 30}, {'cmd': 'spend_savings', 'payload': {'buy_realty': 1800000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 20}, {'cmd': 'spend_savings', 'payload': {'repairing': 1000000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 3}], 'result': {'months': 53, 'savings': 150000, 'criteria': 51.5}}}, 'combs': {(28, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}}, (28, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}}, (28, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}}, (28, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}}, (28, 4): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}}, (28, 5): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}}, (28, 6): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}}, (28, 7): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}}, (28, 8): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}}, (28, 9): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}}, (28, 10): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}}, (28, 11): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1700000, 'need': 1800000}}}, (29, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}}, (29, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}}, (29, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}}, (29, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}}, (29, 4): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}}, (29, 5): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}}, (29, 6): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}}, (29, 7): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}}, (29, 8): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}}, (29, 9): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}}, (29, 10): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}}, (29, 11): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1750000, 'need': 1800000}}}, (30, 0): {'credit': {'success': True, 'months': 49, 'savings': 47448.836667370124, 'criteria': 48.525511633326296}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 700000, 'need': 1000000}}}, (30, 1): {'credit': {'success': True, 'months': 50, 'savings': 98119.06838372255, 'criteria': 49.01880931616277}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 750000, 'need': 1000000}}}, (30, 2): {'credit': {'success': True, 'months': 50, 'savings': 48659.54132321575, 'criteria': 49.51340458676784}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 800000, 'need': 1000000}}}, (30, 3): {'credit': {'success': True, 'months': 51, 'savings': 99195.01178500337, 'criteria': 50.00804988214997}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 850000, 'need': 1000000}}}, (30, 4): {'credit': {'success': True, 'months': 51, 'savings': 49598.220325923234, 'criteria': 50.50401779674077}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (30, 5): {'credit': {'success': True, 'months': 52, 'savings': 99866.31111110977, 'criteria': 51.0013368888889}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (30, 6): {'savings': {'success': True, 'months': 53, 'savings': 150000, 'criteria': 51.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (30, 7): {'savings': {'success': True, 'months': 54, 'savings': 200000, 'criteria': 52.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (30, 8): {'savings': {'success': True, 'months': 55, 'savings': 250000, 'criteria': 52.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (30, 9): {'savings': {'success': True, 'months': 56, 'savings': 300000, 'criteria': 53.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (30, 10): {'savings': {'success': True, 'months': 57, 'savings': 350000, 'criteria': 53.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (30, 11): {'savings': {'success': True, 'months': 58, 'savings': 400000, 'criteria': 54.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (31, 0): {'credit': {'success': True, 'months': 49, 'savings': 47448.836667370124, 'criteria': 48.525511633326296}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 700000, 'need': 1000000}}}, (31, 1): {'credit': {'success': True, 'months': 50, 'savings': 98119.06838372255, 'criteria': 49.01880931616277}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 750000, 'need': 1000000}}}, (31, 2): {'credit': {'success': True, 'months': 50, 'savings': 48659.54132321575, 'criteria': 49.51340458676784}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 800000, 'need': 1000000}}}, (31, 3): {'credit': {'success': True, 'months': 51, 'savings': 99195.01178500337, 'criteria': 50.00804988214997}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 850000, 'need': 1000000}}}, (31, 4): {'credit': {'success': True, 'months': 51, 'savings': 49598.220325923234, 'criteria': 50.50401779674077}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (31, 5): {'credit': {'success': True, 'months': 52, 'savings': 99866.31111110977, 'criteria': 51.0013368888889}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (31, 6): {'savings': {'success': True, 'months': 53, 'savings': 150000, 'criteria': 51.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (31, 7): {'savings': {'success': True, 'months': 54, 'savings': 200000, 'criteria': 52.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (31, 8): {'savings': {'success': True, 'months': 55, 'savings': 250000, 'criteria': 52.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (31, 9): {'savings': {'success': True, 'months': 56, 'savings': 300000, 'criteria': 53.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (31, 10): {'savings': {'success': True, 'months': 57, 'savings': 350000, 'criteria': 53.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (31, 11): {'savings': {'success': True, 'months': 58, 'savings': 400000, 'criteria': 54.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (32, 0): {'credit': {'success': True, 'months': 49, 'savings': 47448.836667370124, 'criteria': 48.525511633326296}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 700000, 'need': 1000000}}}, (32, 1): {'credit': {'success': True, 'months': 50, 'savings': 98119.06838372255, 'criteria': 49.01880931616277}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 750000, 'need': 1000000}}}, (32, 2): {'credit': {'success': True, 'months': 50, 'savings': 48659.54132321575, 'criteria': 49.51340458676784}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 800000, 'need': 1000000}}}, (32, 3): {'credit': {'success': True, 'months': 51, 'savings': 99195.01178500337, 'criteria': 50.00804988214997}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 850000, 'need': 1000000}}}, (32, 4): {'credit': {'success': True, 'months': 51, 'savings': 49598.220325923234, 'criteria': 50.50401779674077}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (32, 5): {'credit': {'success': True, 'months': 52, 'savings': 99866.31111110977, 'criteria': 51.0013368888889}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (32, 6): {'savings': {'success': True, 'months': 53, 'savings': 150000, 'criteria': 51.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (32, 7): {'savings': {'success': True, 'months': 54, 'savings': 200000, 'criteria': 52.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (32, 8): {'savings': {'success': True, 'months': 55, 'savings': 250000, 'criteria': 52.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (32, 9): {'savings': {'success': True, 'months': 56, 'savings': 300000, 'criteria': 53.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (32, 10): {'savings': {'success': True, 'months': 57, 'savings': 350000, 'criteria': 53.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (32, 11): {'savings': {'success': True, 'months': 58, 'savings': 400000, 'criteria': 54.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (33, 0): {'credit': {'success': True, 'months': 49, 'savings': 47448.836667370124, 'criteria': 48.525511633326296}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 700000, 'need': 1000000}}}, (33, 1): {'credit': {'success': True, 'months': 50, 'savings': 98119.06838372255, 'criteria': 49.01880931616277}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 750000, 'need': 1000000}}}, (33, 2): {'credit': {'success': True, 'months': 50, 'savings': 48659.54132321575, 'criteria': 49.51340458676784}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 800000, 'need': 1000000}}}, (33, 3): {'credit': {'success': True, 'months': 51, 'savings': 99195.01178500337, 'criteria': 50.00804988214997}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 850000, 'need': 1000000}}}, (33, 4): {'credit': {'success': True, 'months': 51, 'savings': 49598.220325923234, 'criteria': 50.50401779674077}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (33, 5): {'credit': {'success': True, 'months': 52, 'savings': 99866.31111110977, 'criteria': 51.0013368888889}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (33, 6): {'savings': {'success': True, 'months': 53, 'savings': 150000, 'criteria': 51.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (33, 7): {'savings': {'success': True, 'months': 54, 'savings': 200000, 'criteria': 52.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (33, 8): {'savings': {'success': True, 'months': 55, 'savings': 250000, 'criteria': 52.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (33, 9): {'savings': {'success': True, 'months': 56, 'savings': 300000, 'criteria': 53.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (33, 10): {'savings': {'success': True, 'months': 57, 'savings': 350000, 'criteria': 53.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (33, 11): {'savings': {'success': True, 'months': 58, 'savings': 400000, 'criteria': 54.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (34, 0): {'credit': {'success': True, 'months': 49, 'savings': 47448.836667370124, 'criteria': 48.525511633326296}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 700000, 'need': 1000000}}}, (34, 1): {'credit': {'success': True, 'months': 50, 'savings': 98119.06838372255, 'criteria': 49.01880931616277}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 750000, 'need': 1000000}}}, (34, 2): {'credit': {'success': True, 'months': 50, 'savings': 48659.54132321575, 'criteria': 49.51340458676784}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 800000, 'need': 1000000}}}, (34, 3): {'credit': {'success': True, 'months': 51, 'savings': 99195.01178500337, 'criteria': 50.00804988214997}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 850000, 'need': 1000000}}}, (34, 4): {'credit': {'success': True, 'months': 51, 'savings': 49598.220325923234, 'criteria': 50.50401779674077}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (34, 5): {'credit': {'success': True, 'months': 52, 'savings': 99866.31111110977, 'criteria': 51.0013368888889}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (34, 6): {'savings': {'success': True, 'months': 53, 'savings': 150000, 'criteria': 51.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (34, 7): {'savings': {'success': True, 'months': 54, 'savings': 200000, 'criteria': 52.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (34, 8): {'savings': {'success': True, 'months': 55, 'savings': 250000, 'criteria': 52.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (34, 9): {'savings': {'success': True, 'months': 56, 'savings': 300000, 'criteria': 53.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (34, 10): {'savings': {'success': True, 'months': 57, 'savings': 350000, 'criteria': 53.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (34, 11): {'savings': {'success': True, 'months': 58, 'savings': 400000, 'criteria': 54.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}}}, 32342390239: {'opt': {'savings': {'case': 'S_i < C, S < S_t', 'x': (34, 0), 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 300000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 34}, {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 389003.02, 'schedule': [{'interest_rate': 0.005416666666666667, 'months': 12}, {'interest_rate': 0.008333333333333333, 'months': 228}], 'minimal_initial_sum': 270000.0, 'expenses': 60000}}, {'cmd': 'spend_savings', 'payload': {'buy_realty': 1800000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000, 'to_save': 47099.698}, 'times': 10}, {'cmd': 'spend_savings', 'payload': {'repairing': 1000000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000}, 'times': 3}], 'result': {'months': 50, 'savings': 59415.38984723068, 'criteria': 49.40584610152769}}}, 'combs': {(28, 0): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 50, 'savings': 37256.465042146636, 'criteria': 49.62743534957853}}, (28, 1): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 51, 'savings': 92183.80446897371, 'criteria': 50.078161955310264}}, (28, 2): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 51, 'savings': 47765.10201623985, 'criteria': 50.5223489798376}}, (28, 3): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 51, 'savings': 4322.304127827796, 'criteria': 50.95677695872172}}, (28, 4): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 52, 'savings': 61549.86194401605, 'criteria': 51.38450138055984}}, (28, 5): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 52, 'savings': 19579.89992770207, 'criteria': 51.804201000722976}}, (28, 6): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 53, 'savings': 77769.82543643455, 'criteria': 52.222301745635654}}, (28, 7): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 54, 'savings': 136121.3836465419, 'criteria': 52.63878616353458}}, (28, 8): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 55, 'savings': 188135.94081784695, 'criteria': 53.11864059182153}}, (28, 9): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 56, 'savings': 238135.94081784686, 'criteria': 53.61864059182153}}, (28, 10): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 57, 'savings': 288135.94081784686, 'criteria': 54.11864059182153}}, (28, 11): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 58, 'savings': 338135.94081784686, 'criteria': 54.61864059182153}}, (29, 0): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 50, 'savings': 41181.29387143505, 'criteria': 49.58818706128565}}, (29, 1): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 51, 'savings': 95633.8102302889, 'criteria': 50.04366189769711}}, (29, 2): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 51, 'savings': 50695.14312231165, 'criteria': 50.493048568776885}}, (29, 3): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 51, 'savings': 6719.318933404284, 'criteria': 50.93280681066596}}, (29, 4): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 52, 'savings': 63415.545513617704, 'criteria': 51.365844544863826}}, (29, 5): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 52, 'savings': 21050.70680561283, 'criteria': 51.78949293194387}}, (29, 6): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 53, 'savings': 79043.28633295538, 'criteria': 52.209567136670444}}, (29, 7): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 54, 'savings': 137195.77409676163, 'criteria': 52.628042259032384}}, (29, 8): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 55, 'savings': 189016.76724030587, 'criteria': 53.10983232759694}}, (29, 9): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 56, 'savings': 239016.76724030587, 'criteria': 53.60983232759694}}, (29, 10): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 57, 'savings': 289016.7672403059, 'criteria': 54.10983232759694}}, (29, 11): {'credit': {'success': False, 'code': 'MRG_MP', 'payload': {'need': 84784.82998637267, 'have': 50000}}, 'savings': {'success': True, 'months': 58, 'savings': 339016.7672403059, 'criteria': 54.60983232759694}}, (30, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 50, 'savings': 45027.07736493771, 'criteria': 49.54972922635062}}, (30, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 51, 'savings': 99013.52427412575, 'criteria': 50.00986475725874}}, (30, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 51, 'savings': 53564.78308740028, 'criteria': 50.464352169125995}}, (30, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 51, 'savings': 9066.33201953974, 'criteria': 50.9093366798046}}, (30, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 52, 'savings': 65241.84247880203, 'criteria': 51.34758157521198}}, (30, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 52, 'savings': 22322.763808587883, 'criteria': 51.77677236191412}}, (30, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (30, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (30, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (30, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (30, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (30, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (31, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 50, 'savings': 48796.2913964655, 'criteria': 49.51203708603534}}, (31, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 50, 'savings': 2305.9547941912024, 'criteria': 49.97694045205809}}, (31, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 51, 'savings': 56375.95490444678, 'criteria': 50.43624045095553}}, (31, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 51, 'savings': 11364.962620864608, 'criteria': 50.88635037379135}}, (31, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 52, 'savings': 67030.04296969288, 'criteria': 51.32969957030307}}, (31, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 52, 'savings': 23567.966477771177, 'criteria': 51.76432033522229}}, (31, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (31, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (31, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (31, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (31, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (31, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (32, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 50, 'savings': 52491.30726679368, 'criteria': 49.475086927332065}}, (32, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 50, 'savings': 5524.8427807395055, 'criteria': 49.94475157219261}}, (32, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 51, 'savings': 59130.5105487749, 'criteria': 50.40869489451225}}, (32, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 51, 'savings': 13616.759969373728, 'criteria': 50.863832400306265}}, (32, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 52, 'savings': 68781.380480341, 'criteria': 51.31218619519659}}, (32, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 52, 'savings': 24787.1938822037, 'criteria': 51.75212806117796}}, (32, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (32, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (32, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (32, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (32, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (32, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (33, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 50, 'savings': 56054.1762203277, 'criteria': 49.43945823779672}}, (33, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 50, 'savings': 8680.366198531643, 'criteria': 49.913196338014686}}, (33, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 51, 'savings': 61830.22320450298, 'criteria': 50.38169776795497}}, (33, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 51, 'savings': 15823.206251263677, 'criteria': 50.84176793748736}}, (33, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 52, 'savings': 70497.03575040057, 'criteria': 51.295029642496}}, (33, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 52, 'savings': 25981.28799688678, 'criteria': 51.74018712003113}}, (33, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (33, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (33, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (33, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (33, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (33, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (34, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 50, 'savings': 59415.38984723068, 'criteria': 49.40584610152769}}, (34, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 50, 'savings': 11723.036473289103, 'criteria': 49.88276963526711}}, (34, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 51, 'savings': 64476.79121406369, 'criteria': 50.35523208785936}}, (34, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 51, 'savings': 17985.72252512329, 'criteria': 50.82014277474877}}, (34, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 52, 'savings': 72178.13757812396, 'criteria': 51.27821862421876}}, (34, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 52, 'savings': 27151.052626643464, 'criteria': 51.72848947373357}}, (34, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (34, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (34, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (34, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (34, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (34, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}}}}},
        "expanded": {
            83927978940983: {
                'no_mortgage': {
                    'savings': [
                        {
                            "after_savings": 300000,
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 300000,
                                "irregular_income": 0,
                                "regular_income": 0
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": 350000,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 0,
                                    "after_closing_payment": 0,
                                    "after_debt": 0,
                                    "after_minimal_payment": 0,
                                    "before_closing_payment": 0,
                                    "before_debt": 0,
                                    "before_minimal_payment": 0,
                                    "interest_rate": 0
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 300000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 400000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 350000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 450000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 400000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 500000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 450000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 550000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 500000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 600000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 550000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 650000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 600000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 700000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 650000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 750000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 700000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 800000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 750000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 850000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 800000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 900000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 850000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 950000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 900000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1000000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 950000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1050000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1000000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1100000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1050000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1150000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1100000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1200000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1150000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1250000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1200000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1300000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1250000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1350000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1300000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1400000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1350000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1450000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1400000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1500000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1450000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1550000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1500000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1600000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1550000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1650000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1600000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1700000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1650000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1750000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1700000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 0,
                            "expenses": {
                                "irregular_expenses": 1850000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1750000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 50000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 0,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 100000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 50000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 150000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 100000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 200000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 150000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 250000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 200000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 300000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 250000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 350000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 300000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 400000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 350000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 450000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 400000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 500000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 450000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 550000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 500000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 600000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 550000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 650000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 600000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 700000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 650000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 750000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 700000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 800000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 750000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 850000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 800000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 900000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 850000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 950000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 900000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 0,
                            "expenses": {
                                "irregular_expenses": 1050000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 950000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 50000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 0,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 100000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 50000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 150000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 100000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        }
                    ],
                    'credit': [
                        {
                            "after_savings": 300000,
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 300000,
                                "irregular_income": 0,
                                "regular_income": 0
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": 350000,
                            "debts": {
                                "Mortgage": {
                                    "actual_payment": 0,
                                    "after_closing_payment": 0,
                                    "after_debt": 0,
                                    "after_minimal_payment": 0,
                                    "before_closing_payment": 0,
                                    "before_debt": 0,
                                    "before_minimal_payment": 0,
                                    "interest_rate": 0
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 300000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 400000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 350000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 450000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 400000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 500000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 450000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 550000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 500000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 600000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 550000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 650000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 600000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 700000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 650000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 750000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 700000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 800000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 750000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 850000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 800000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 900000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 850000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 950000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 900000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1000000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 950000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1050000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1000000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1100000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1050000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1150000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1100000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1200000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1150000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1250000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1200000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1300000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1250000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1350000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1300000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1400000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1350000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1450000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1400000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1500000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1450000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1550000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1500000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1600000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1550000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1650000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1600000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1700000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1650000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 1750000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1700000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 0,
                            "expenses": {
                                "irregular_expenses": 1850000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 1750000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 50000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 0,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 100000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 50000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 150000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 100000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 200000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 150000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 250000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 200000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 300000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 250000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 350000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 300000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 400000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 350000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 450000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 400000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 500000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 450000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 550000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 500000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 600000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 550000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 650000,
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 600000,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 0,
                            "debts": {
                                "Repairing": {
                                    "actual_payment": 0,
                                    "after_closing_payment": 300800.0,
                                    "after_debt": 300000,
                                    "after_minimal_payment": 25435.448995911804,
                                    "before_closing_payment": 0,
                                    "before_debt": 0,
                                    "before_minimal_payment": 0,
                                    "interest_rate": 0.002666666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 1050000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 650000,
                                "irregular_income": 300000,
                                "regular_income": 100000
                            },
                            "last_save": 50000,
                            "to_save": 0
                        },
                        {
                            "after_savings": 0,
                            "debts": {
                                "Repairing": {
                                    "actual_payment": 50000.0,
                                    "after_closing_payment": 251468.80000000136,
                                    "after_debt": 250800.00000000134,
                                    "after_minimal_payment": 23166.41915204242,
                                    "before_closing_payment": 300800.0,
                                    "before_debt": 300000,
                                    "before_minimal_payment": 25435.448995911804,
                                    "interest_rate": 0.002666666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 0,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": 0,
                            "debts": {
                                "Repairing": {
                                    "actual_payment": 50000.0,
                                    "after_closing_payment": 202006.05013333602,
                                    "after_debt": 201468.80000000267,
                                    "after_minimal_payment": 20443.54793639806,
                                    "before_closing_payment": 251468.80000000136,
                                    "before_debt": 250800.00000000134,
                                    "before_minimal_payment": 23166.41915204242,
                                    "interest_rate": 0.002666666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 0,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": 0,
                            "debts": {
                                "Repairing": {
                                    "actual_payment": 50000.0,
                                    "after_closing_payment": 152411.39960035955,
                                    "after_debt": 152006.05013333732,
                                    "after_minimal_payment": 17115.554891134536,
                                    "before_closing_payment": 202006.05013333602,
                                    "before_debt": 201468.80000000267,
                                    "before_minimal_payment": 20443.54793639806,
                                    "interest_rate": 0.002666666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 50000,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 0,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": 0,
                            "debts": {
                                "Repairing": {
                                    "actual_payment": 100000.0,
                                    "after_closing_payment": 52551.163332629876,
                                    "after_debt": 52411.399600362245,
                                    "after_minimal_payment": 6630.286308564044,
                                    "before_closing_payment": 152411.39960035955,
                                    "before_debt": 152006.05013333732,
                                    "before_minimal_payment": 17115.554891134536,
                                    "interest_rate": 0.002666666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 0,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 0,
                            "to_save": 0
                        },
                        {
                            "after_savings": 47448.836667370124,
                            "debts": {
                                "Repairing": {
                                    "actual_payment": 52551.163332629876,
                                    "after_closing_payment": 0,
                                    "after_debt": 0,
                                    "after_minimal_payment": 0,
                                    "before_closing_payment": 52551.163332629876,
                                    "before_debt": 52411.399600362245,
                                    "before_minimal_payment": 6630.286308564044,
                                    "interest_rate": 0.002666666666666667
                                }
                            },
                            "expenses": {
                                "irregular_expenses": 0,
                                "regular_expenses": 0
                            },
                            "incomes": {
                                "before_savings": 0,
                                "irregular_income": 0,
                                "regular_income": 100000
                            },
                            "last_save": 47448.836667370124,
                            "to_save": 0
                        }
                    ],
                },
                32342390239: {
                    'savings': [{'debts': {}, 'incomes': {'before_savings': 300000, 'regular_income': 0, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0}, {'debts': {}, 'incomes': {'before_savings': 300000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 350000}, {'debts': {}, 'incomes': {'before_savings': 350000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 400000}, {'debts': {}, 'incomes': {'before_savings': 400000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 450000}, {'debts': {}, 'incomes': {'before_savings': 450000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 500000}, {'debts': {}, 'incomes': {'before_savings': 500000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 550000}, {'debts': {}, 'incomes': {'before_savings': 550000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 600000}, {'debts': {}, 'incomes': {'before_savings': 600000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 650000}, {'debts': {}, 'incomes': {'before_savings': 650000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 700000}, {'debts': {}, 'incomes': {'before_savings': 700000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 750000}, {'debts': {}, 'incomes': {'before_savings': 750000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 800000}, {'debts': {}, 'incomes': {'before_savings': 800000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 850000}, {'debts': {}, 'incomes': {'before_savings': 850000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 900000}, {'debts': {}, 'incomes': {'before_savings': 900000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 950000}, {'debts': {}, 'incomes': {'before_savings': 950000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1000000}, {'debts': {}, 'incomes': {'before_savings': 1000000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1050000}, {'debts': {}, 'incomes': {'before_savings': 1050000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1100000}, {'debts': {}, 'incomes': {'before_savings': 1100000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1150000}, {'debts': {}, 'incomes': {'before_savings': 1150000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1200000}, {'debts': {}, 'incomes': {'before_savings': 1200000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1250000}, {'debts': {}, 'incomes': {'before_savings': 1250000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1300000}, {'debts': {}, 'incomes': {'before_savings': 1300000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1350000}, {'debts': {}, 'incomes': {'before_savings': 1350000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1400000}, {'debts': {}, 'incomes': {'before_savings': 1400000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1450000}, {'debts': {}, 'incomes': {'before_savings': 1450000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1500000}, {'debts': {}, 'incomes': {'before_savings': 1500000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1550000}, {'debts': {}, 'incomes': {'before_savings': 1550000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1600000}, {'debts': {}, 'incomes': {'before_savings': 1600000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1650000}, {'debts': {}, 'incomes': {'before_savings': 1650000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1700000}, {'debts': {}, 'incomes': {'before_savings': 1700000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1750000}, {'debts': {}, 'incomes': {'before_savings': 1750000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1800000}, {'debts': {}, 'incomes': {'before_savings': 1800000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1850000}, {'debts': {}, 'incomes': {'before_savings': 1850000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1900000}, {'debts': {}, 'incomes': {'before_savings': 1900000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 1950000}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 0, 'before_minimal_payment': 0, 'before_closing_payment': 0, 'actual_payment': 0, 'after_debt': 389003.02, 'after_minimal_payment': 2900.302013462431, 'after_closing_payment': 391110.1196916667}}, 'incomes': {'before_savings': 1950000, 'regular_income': 100000, 'irregular_income': 389003.02}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'mortgage_expenses': 60000, 'buy_realty': 1800000}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 529003.02}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 389003.02, 'before_minimal_payment': 2900.302013462431, 'before_closing_payment': 391110.1196916667, 'actual_payment': 2900.3020000000033, 'after_debt': 388209.8176916667, 'after_minimal_payment': 2900.3020135630086, 'after_closing_payment': 390312.6208708299}}, 'incomes': {'before_savings': 529003.02, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 47099.698, 'last_save': 0, 'after_savings': 576102.718}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 388209.8176916667, 'before_minimal_payment': 2900.3020135630086, 'before_closing_payment': 390312.6208708299, 'actual_payment': 2900.3020135630086, 'after_debt': 387412.3188572669, 'after_minimal_payment': 2900.302013563009, 'after_closing_payment': 389510.80225107714}}, 'incomes': {'before_savings': 576102.718, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 47099.69798643699, 'last_save': 0, 'after_savings': 623202.415986437}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 387412.3188572669, 'before_minimal_payment': 2900.302013563009, 'before_closing_payment': 389510.80225107714, 'actual_payment': 2900.302013563009, 'after_debt': 386610.5002375141, 'after_minimal_payment': 2900.302013563009, 'after_closing_payment': 388704.640447134}}, 'incomes': {'before_savings': 623202.415986437, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 47099.69798643699, 'last_save': 0, 'after_savings': 670302.113972874}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 386610.5002375141, 'before_minimal_payment': 2900.302013563009, 'before_closing_payment': 388704.640447134, 'actual_payment': 2900.302013563009, 'after_debt': 385804.3384335709, 'after_minimal_payment': 2900.3020135630086, 'after_closing_payment': 387894.1119334194}}, 'incomes': {'before_savings': 670302.113972874, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 47099.69798643699, 'last_save': 0, 'after_savings': 717401.811959311}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 385804.3384335709, 'before_minimal_payment': 2900.3020135630086, 'before_closing_payment': 387894.1119334194, 'actual_payment': 2900.3020135630086, 'after_debt': 384993.80991985643, 'after_minimal_payment': 2900.302013563008, 'after_closing_payment': 387079.19305692235}}, 'incomes': {'before_savings': 717401.811959311, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 47099.69798643699, 'last_save': 0, 'after_savings': 764501.509945748}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 384993.80991985643, 'before_minimal_payment': 2900.302013563008, 'before_closing_payment': 387079.19305692235, 'actual_payment': 2900.302013563008, 'after_debt': 384178.8910433593, 'after_minimal_payment': 2900.302013563008, 'after_closing_payment': 386259.8600365108}}, 'incomes': {'before_savings': 764501.509945748, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 47099.69798643699, 'last_save': 0, 'after_savings': 811601.207932185}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 384178.8910433593, 'before_minimal_payment': 2900.302013563008, 'before_closing_payment': 386259.8600365108, 'actual_payment': 2900.302013563008, 'after_debt': 383359.5580229478, 'after_minimal_payment': 2900.3020135630086, 'after_closing_payment': 385436.08896223875}}, 'incomes': {'before_savings': 811601.207932185, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 47099.69798643699, 'last_save': 0, 'after_savings': 858700.905918622}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 383359.5580229478, 'before_minimal_payment': 2900.3020135630086, 'before_closing_payment': 385436.08896223875, 'actual_payment': 2900.3020135630086, 'after_debt': 382535.78694867576, 'after_minimal_payment': 2900.3020135630077, 'after_closing_payment': 384607.85579464777}}, 'incomes': {'before_savings': 858700.905918622, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 47099.69798643699, 'last_save': 0, 'after_savings': 905800.603905059}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 382535.78694867576, 'before_minimal_payment': 2900.3020135630077, 'before_closing_payment': 384607.85579464777, 'actual_payment': 2900.3020135630077, 'after_debt': 381707.5537810847, 'after_minimal_payment': 2900.3020135630077, 'after_closing_payment': 383775.1363640656}}, 'incomes': {'before_savings': 905800.603905059, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 47099.69798643699, 'last_save': 0, 'after_savings': 952900.301891496}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 381707.5537810847, 'before_minimal_payment': 2900.3020135630077, 'before_closing_payment': 383775.1363640656, 'actual_payment': 2900.3020135630077, 'after_debt': 380874.8343505026, 'after_minimal_payment': 2900.302013563008, 'after_closing_payment': 382937.90636990115}}, 'incomes': {'before_savings': 952900.301891496, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'repairing': 1000000}}, 'to_save': 47099.69798643699, 'last_save': 0, 'after_savings': -0.00012206693645566702}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 380874.8343505026, 'before_minimal_payment': 2900.302013563008, 'before_closing_payment': 382937.90636990115, 'actual_payment': 50000.0, 'after_debt': 332937.90636990155, 'after_minimal_payment': 2540.855086884176, 'after_closing_payment': 334741.3200294052}}, 'incomes': {'before_savings': -0.00012206693645566702, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.00012206693645566702}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 332937.90636990155, 'before_minimal_payment': 2540.855086884176, 'before_closing_payment': 334741.3200294052, 'actual_payment': 50000.0, 'after_debt': 284741.3200294056, 'after_minimal_payment': 2794.0495834733906, 'after_closing_payment': 287114.16436298395}}, 'incomes': {'before_savings': -0.00012206693645566702, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.00012206693645566702}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 284741.3200294056, 'before_minimal_payment': 2794.0495834733906, 'before_closing_payment': 287114.16436298395, 'actual_payment': 50000.0, 'after_debt': 237114.16436298413, 'after_minimal_payment': 2330.1507622413924, 'after_closing_payment': 239090.11573267565}}, 'incomes': {'before_savings': -0.00012206693645566702, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.00012206693645566702}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 237114.16436298413, 'before_minimal_payment': 2330.1507622413924, 'before_closing_payment': 239090.11573267565, 'actual_payment': 100000.0, 'after_debt': 139090.115732676, 'after_minimal_payment': 1368.9009425018753, 'after_closing_payment': 140249.2000304483}}, 'incomes': {'before_savings': -0.00012206693645566702, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.00012206693645566702}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 139090.115732676, 'before_minimal_payment': 1368.9009425018753, 'before_closing_payment': 140249.2000304483, 'actual_payment': 100000.0, 'after_debt': 40249.20003044864, 'after_minimal_payment': 396.7241445940233, 'after_closing_payment': 40584.61003070238}}, 'incomes': {'before_savings': -0.00012206693645566702, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.00012206693645566702}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 40249.20003044864, 'before_minimal_payment': 396.7241445940233, 'before_closing_payment': 40584.61003070238, 'actual_payment': 40584.61003070238, 'after_debt': 0, 'after_minimal_payment': 0, 'after_closing_payment': 0}}, 'incomes': {'before_savings': -0.00012206693645566702, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 59415.38996929762, 'after_savings': 59415.38984723068}],
                }
            }
        }
    },
    "ex__SuperGood__x_1__0": {
        "input_data": {
            "personal_data": {
                "current_savings": 2000000,
                "month_income": 120000,
                "month_rent": 50000,
                "month_expenses": 0,
                "deal_month_start": 0,
                "deal_month_finish": 12,
                "max_repairing_delay_months": 12,
                "inflation_percent": 0,
            },
            "credit_scheme": {
                "interest_rate": 3.2 / 1200,
                "months": 12,
            },
            "mortgage_schemes": {
                32342390239: {
                    "id": 32342390239,
                    "title": "12345678901234567890",
                    "initial_payment_percent": 15,
                    "initial_expenses": 60000,
                    "schedule": [
                        {
                            "interest_rate": 6.5 / 1200,
                            "months": 12,
                        },
                        {
                            "interest_rate": 10 / 1200,
                            "months": 228,
                        }
                    ]
                }
            },
            "realties": {
                83927978940983: {
                    "id": 83927978940983,
                    "title": "12345678901234567890",
                    "link": "https://yandex.ru",
                    "cost": 1800000,
                    "is_primary": True,
                    "get_keys_month": 14,
                    "repairing_expenses": 1000000,
                    "repairing_months": 3,
                    "area": None,
                    "subway_distance": None,
                    "has_mall": None,
                }
            }
        },
        "output_data": {83927978940983: {'no_mortgage': {'opt': {'savings': {'case': 'S_i > C, S > S_t', 'x': (0, 0), 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 2000000}}, {'cmd': 'spend_savings', 'payload': {'buy_realty': 1800000}}, {'cmd': 'make_month', 'payload': {'earned': 120000, 'month_rent': 50000}, 'times': 14}, {'cmd': 'spend_savings', 'payload': {'repairing': 1000000}}, {'cmd': 'make_month', 'payload': {'earned': 120000, 'month_rent': 50000}, 'times': 3}], 'result': {'months': 17, 'savings': 390000, 'criteria': 13.75}}}, 'combs': {(0, 0): {'savings': {'success': True, 'months': 17, 'savings': 390000, 'criteria': 13.75}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 1): {'savings': {'success': True, 'months': 18, 'savings': 460000, 'criteria': 14.166666666666666}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 2): {'savings': {'success': True, 'months': 19, 'savings': 530000, 'criteria': 14.583333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 3): {'savings': {'success': True, 'months': 20, 'savings': 600000, 'criteria': 15.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 4): {'savings': {'success': True, 'months': 21, 'savings': 670000, 'criteria': 15.416666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 5): {'savings': {'success': True, 'months': 22, 'savings': 740000, 'criteria': 15.833333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 6): {'savings': {'success': True, 'months': 23, 'savings': 810000, 'criteria': 16.25}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 7): {'savings': {'success': True, 'months': 24, 'savings': 880000, 'criteria': 16.666666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 8): {'savings': {'success': True, 'months': 25, 'savings': 950000, 'criteria': 17.083333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 9): {'savings': {'success': True, 'months': 26, 'savings': 1020000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 10): {'savings': {'success': True, 'months': 27, 'savings': 1090000, 'criteria': 17.916666666666664}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 11): {'savings': {'success': True, 'months': 28, 'savings': 1160000, 'criteria': 18.333333333333336}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 0): {'savings': {'success': True, 'months': 17, 'savings': 390000, 'criteria': 13.75}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 1): {'savings': {'success': True, 'months': 18, 'savings': 460000, 'criteria': 14.166666666666666}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 2): {'savings': {'success': True, 'months': 19, 'savings': 530000, 'criteria': 14.583333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 3): {'savings': {'success': True, 'months': 20, 'savings': 600000, 'criteria': 15.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 4): {'savings': {'success': True, 'months': 21, 'savings': 670000, 'criteria': 15.416666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 5): {'savings': {'success': True, 'months': 22, 'savings': 740000, 'criteria': 15.833333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 6): {'savings': {'success': True, 'months': 23, 'savings': 810000, 'criteria': 16.25}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 7): {'savings': {'success': True, 'months': 24, 'savings': 880000, 'criteria': 16.666666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 8): {'savings': {'success': True, 'months': 25, 'savings': 950000, 'criteria': 17.083333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 9): {'savings': {'success': True, 'months': 26, 'savings': 1020000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 10): {'savings': {'success': True, 'months': 27, 'savings': 1090000, 'criteria': 17.916666666666664}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 11): {'savings': {'success': True, 'months': 28, 'savings': 1160000, 'criteria': 18.333333333333336}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 0): {'savings': {'success': True, 'months': 17, 'savings': 390000, 'criteria': 13.75}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 1): {'savings': {'success': True, 'months': 18, 'savings': 460000, 'criteria': 14.166666666666666}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 2): {'savings': {'success': True, 'months': 19, 'savings': 530000, 'criteria': 14.583333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 3): {'savings': {'success': True, 'months': 20, 'savings': 600000, 'criteria': 15.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 4): {'savings': {'success': True, 'months': 21, 'savings': 670000, 'criteria': 15.416666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 5): {'savings': {'success': True, 'months': 22, 'savings': 740000, 'criteria': 15.833333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 6): {'savings': {'success': True, 'months': 23, 'savings': 810000, 'criteria': 16.25}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 7): {'savings': {'success': True, 'months': 24, 'savings': 880000, 'criteria': 16.666666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 8): {'savings': {'success': True, 'months': 25, 'savings': 950000, 'criteria': 17.083333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 9): {'savings': {'success': True, 'months': 26, 'savings': 1020000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 10): {'savings': {'success': True, 'months': 27, 'savings': 1090000, 'criteria': 17.916666666666664}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 11): {'savings': {'success': True, 'months': 28, 'savings': 1160000, 'criteria': 18.333333333333336}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 0): {'savings': {'success': True, 'months': 17, 'savings': 390000, 'criteria': 13.75}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 1): {'savings': {'success': True, 'months': 18, 'savings': 460000, 'criteria': 14.166666666666666}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 2): {'savings': {'success': True, 'months': 19, 'savings': 530000, 'criteria': 14.583333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 3): {'savings': {'success': True, 'months': 20, 'savings': 600000, 'criteria': 15.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 4): {'savings': {'success': True, 'months': 21, 'savings': 670000, 'criteria': 15.416666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 5): {'savings': {'success': True, 'months': 22, 'savings': 740000, 'criteria': 15.833333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 6): {'savings': {'success': True, 'months': 23, 'savings': 810000, 'criteria': 16.25}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 7): {'savings': {'success': True, 'months': 24, 'savings': 880000, 'criteria': 16.666666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 8): {'savings': {'success': True, 'months': 25, 'savings': 950000, 'criteria': 17.083333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 9): {'savings': {'success': True, 'months': 26, 'savings': 1020000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 10): {'savings': {'success': True, 'months': 27, 'savings': 1090000, 'criteria': 17.916666666666664}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 11): {'savings': {'success': True, 'months': 28, 'savings': 1160000, 'criteria': 18.333333333333336}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 0): {'savings': {'success': True, 'months': 17, 'savings': 390000, 'criteria': 13.75}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 1): {'savings': {'success': True, 'months': 18, 'savings': 460000, 'criteria': 14.166666666666666}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 2): {'savings': {'success': True, 'months': 19, 'savings': 530000, 'criteria': 14.583333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 3): {'savings': {'success': True, 'months': 20, 'savings': 600000, 'criteria': 15.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 4): {'savings': {'success': True, 'months': 21, 'savings': 670000, 'criteria': 15.416666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 5): {'savings': {'success': True, 'months': 22, 'savings': 740000, 'criteria': 15.833333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 6): {'savings': {'success': True, 'months': 23, 'savings': 810000, 'criteria': 16.25}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 7): {'savings': {'success': True, 'months': 24, 'savings': 880000, 'criteria': 16.666666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 8): {'savings': {'success': True, 'months': 25, 'savings': 950000, 'criteria': 17.083333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 9): {'savings': {'success': True, 'months': 26, 'savings': 1020000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 10): {'savings': {'success': True, 'months': 27, 'savings': 1090000, 'criteria': 17.916666666666664}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 11): {'savings': {'success': True, 'months': 28, 'savings': 1160000, 'criteria': 18.333333333333336}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 0): {'savings': {'success': True, 'months': 17, 'savings': 390000, 'criteria': 13.75}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 1): {'savings': {'success': True, 'months': 18, 'savings': 460000, 'criteria': 14.166666666666666}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 2): {'savings': {'success': True, 'months': 19, 'savings': 530000, 'criteria': 14.583333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 3): {'savings': {'success': True, 'months': 20, 'savings': 600000, 'criteria': 15.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 4): {'savings': {'success': True, 'months': 21, 'savings': 670000, 'criteria': 15.416666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 5): {'savings': {'success': True, 'months': 22, 'savings': 740000, 'criteria': 15.833333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 6): {'savings': {'success': True, 'months': 23, 'savings': 810000, 'criteria': 16.25}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 7): {'savings': {'success': True, 'months': 24, 'savings': 880000, 'criteria': 16.666666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 8): {'savings': {'success': True, 'months': 25, 'savings': 950000, 'criteria': 17.083333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 9): {'savings': {'success': True, 'months': 26, 'savings': 1020000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 10): {'savings': {'success': True, 'months': 27, 'savings': 1090000, 'criteria': 17.916666666666664}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 11): {'savings': {'success': True, 'months': 28, 'savings': 1160000, 'criteria': 18.333333333333336}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 0): {'savings': {'success': True, 'months': 17, 'savings': 390000, 'criteria': 13.75}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 1): {'savings': {'success': True, 'months': 18, 'savings': 460000, 'criteria': 14.166666666666666}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 2): {'savings': {'success': True, 'months': 19, 'savings': 530000, 'criteria': 14.583333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 3): {'savings': {'success': True, 'months': 20, 'savings': 600000, 'criteria': 15.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 4): {'savings': {'success': True, 'months': 21, 'savings': 670000, 'criteria': 15.416666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 5): {'savings': {'success': True, 'months': 22, 'savings': 740000, 'criteria': 15.833333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 6): {'savings': {'success': True, 'months': 23, 'savings': 810000, 'criteria': 16.25}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 7): {'savings': {'success': True, 'months': 24, 'savings': 880000, 'criteria': 16.666666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 8): {'savings': {'success': True, 'months': 25, 'savings': 950000, 'criteria': 17.083333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 9): {'savings': {'success': True, 'months': 26, 'savings': 1020000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 10): {'savings': {'success': True, 'months': 27, 'savings': 1090000, 'criteria': 17.916666666666664}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 11): {'savings': {'success': True, 'months': 28, 'savings': 1160000, 'criteria': 18.333333333333336}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 0): {'savings': {'success': True, 'months': 17, 'savings': 390000, 'criteria': 13.75}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 1): {'savings': {'success': True, 'months': 18, 'savings': 460000, 'criteria': 14.166666666666666}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 2): {'savings': {'success': True, 'months': 19, 'savings': 530000, 'criteria': 14.583333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 3): {'savings': {'success': True, 'months': 20, 'savings': 600000, 'criteria': 15.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 4): {'savings': {'success': True, 'months': 21, 'savings': 670000, 'criteria': 15.416666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 5): {'savings': {'success': True, 'months': 22, 'savings': 740000, 'criteria': 15.833333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 6): {'savings': {'success': True, 'months': 23, 'savings': 810000, 'criteria': 16.25}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 7): {'savings': {'success': True, 'months': 24, 'savings': 880000, 'criteria': 16.666666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 8): {'savings': {'success': True, 'months': 25, 'savings': 950000, 'criteria': 17.083333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 9): {'savings': {'success': True, 'months': 26, 'savings': 1020000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 10): {'savings': {'success': True, 'months': 27, 'savings': 1090000, 'criteria': 17.916666666666664}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 11): {'savings': {'success': True, 'months': 28, 'savings': 1160000, 'criteria': 18.333333333333336}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 0): {'savings': {'success': True, 'months': 17, 'savings': 390000, 'criteria': 13.75}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 1): {'savings': {'success': True, 'months': 18, 'savings': 460000, 'criteria': 14.166666666666666}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 2): {'savings': {'success': True, 'months': 19, 'savings': 530000, 'criteria': 14.583333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 3): {'savings': {'success': True, 'months': 20, 'savings': 600000, 'criteria': 15.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 4): {'savings': {'success': True, 'months': 21, 'savings': 670000, 'criteria': 15.416666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 5): {'savings': {'success': True, 'months': 22, 'savings': 740000, 'criteria': 15.833333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 6): {'savings': {'success': True, 'months': 23, 'savings': 810000, 'criteria': 16.25}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 7): {'savings': {'success': True, 'months': 24, 'savings': 880000, 'criteria': 16.666666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 8): {'savings': {'success': True, 'months': 25, 'savings': 950000, 'criteria': 17.083333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 9): {'savings': {'success': True, 'months': 26, 'savings': 1020000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 10): {'savings': {'success': True, 'months': 27, 'savings': 1090000, 'criteria': 17.916666666666664}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 11): {'savings': {'success': True, 'months': 28, 'savings': 1160000, 'criteria': 18.333333333333336}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 0): {'savings': {'success': True, 'months': 17, 'savings': 390000, 'criteria': 13.75}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 1): {'savings': {'success': True, 'months': 18, 'savings': 460000, 'criteria': 14.166666666666666}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 2): {'savings': {'success': True, 'months': 19, 'savings': 530000, 'criteria': 14.583333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 3): {'savings': {'success': True, 'months': 20, 'savings': 600000, 'criteria': 15.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 4): {'savings': {'success': True, 'months': 21, 'savings': 670000, 'criteria': 15.416666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 5): {'savings': {'success': True, 'months': 22, 'savings': 740000, 'criteria': 15.833333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 6): {'savings': {'success': True, 'months': 23, 'savings': 810000, 'criteria': 16.25}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 7): {'savings': {'success': True, 'months': 24, 'savings': 880000, 'criteria': 16.666666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 8): {'savings': {'success': True, 'months': 25, 'savings': 950000, 'criteria': 17.083333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 9): {'savings': {'success': True, 'months': 26, 'savings': 1020000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 10): {'savings': {'success': True, 'months': 27, 'savings': 1090000, 'criteria': 17.916666666666664}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 11): {'savings': {'success': True, 'months': 28, 'savings': 1160000, 'criteria': 18.333333333333336}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 0): {'savings': {'success': True, 'months': 17, 'savings': 390000, 'criteria': 13.75}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 1): {'savings': {'success': True, 'months': 18, 'savings': 460000, 'criteria': 14.166666666666666}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 2): {'savings': {'success': True, 'months': 19, 'savings': 530000, 'criteria': 14.583333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 3): {'savings': {'success': True, 'months': 20, 'savings': 600000, 'criteria': 15.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 4): {'savings': {'success': True, 'months': 21, 'savings': 670000, 'criteria': 15.416666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 5): {'savings': {'success': True, 'months': 22, 'savings': 740000, 'criteria': 15.833333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 6): {'savings': {'success': True, 'months': 23, 'savings': 810000, 'criteria': 16.25}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 7): {'savings': {'success': True, 'months': 24, 'savings': 880000, 'criteria': 16.666666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 8): {'savings': {'success': True, 'months': 25, 'savings': 950000, 'criteria': 17.083333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 9): {'savings': {'success': True, 'months': 26, 'savings': 1020000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 10): {'savings': {'success': True, 'months': 27, 'savings': 1090000, 'criteria': 17.916666666666664}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 11): {'savings': {'success': True, 'months': 28, 'savings': 1160000, 'criteria': 18.333333333333336}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 0): {'savings': {'success': True, 'months': 17, 'savings': 390000, 'criteria': 13.75}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 1): {'savings': {'success': True, 'months': 18, 'savings': 460000, 'criteria': 14.166666666666666}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 2): {'savings': {'success': True, 'months': 19, 'savings': 530000, 'criteria': 14.583333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 3): {'savings': {'success': True, 'months': 20, 'savings': 600000, 'criteria': 15.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 4): {'savings': {'success': True, 'months': 21, 'savings': 670000, 'criteria': 15.416666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 5): {'savings': {'success': True, 'months': 22, 'savings': 740000, 'criteria': 15.833333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 6): {'savings': {'success': True, 'months': 23, 'savings': 810000, 'criteria': 16.25}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 7): {'savings': {'success': True, 'months': 24, 'savings': 880000, 'criteria': 16.666666666666668}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 8): {'savings': {'success': True, 'months': 25, 'savings': 950000, 'criteria': 17.083333333333332}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 9): {'savings': {'success': True, 'months': 26, 'savings': 1020000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 10): {'savings': {'success': True, 'months': 27, 'savings': 1090000, 'criteria': 17.916666666666664}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 11): {'savings': {'success': True, 'months': 28, 'savings': 1160000, 'criteria': 18.333333333333336}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}}}, 32342390239: {'opt': None, 'combs': {(0, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}}}}},
        "expanded": {
            83927978940983: {
                'no_mortgage': {
                    'savings': [{'debts': {}, 'incomes': {'before_savings': 2000000, 'regular_income': 0, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {'buy_realty': 1800000}}, 'to_save': 0, 'last_save': 0, 'after_savings': -1800000}, {'debts': {}, 'incomes': {'before_savings': 200000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 270000}, {'debts': {}, 'incomes': {'before_savings': 270000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 340000}, {'debts': {}, 'incomes': {'before_savings': 340000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 410000}, {'debts': {}, 'incomes': {'before_savings': 410000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 480000}, {'debts': {}, 'incomes': {'before_savings': 480000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 550000}, {'debts': {}, 'incomes': {'before_savings': 550000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 620000}, {'debts': {}, 'incomes': {'before_savings': 620000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 690000}, {'debts': {}, 'incomes': {'before_savings': 690000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 760000}, {'debts': {}, 'incomes': {'before_savings': 760000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 830000}, {'debts': {}, 'incomes': {'before_savings': 830000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 900000}, {'debts': {}, 'incomes': {'before_savings': 900000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 970000}, {'debts': {}, 'incomes': {'before_savings': 970000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 1040000}, {'debts': {}, 'incomes': {'before_savings': 1040000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 1110000}, {'debts': {}, 'incomes': {'before_savings': 1110000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'repairing': 1000000}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 180000}, {'debts': {}, 'incomes': {'before_savings': 180000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 250000}, {'debts': {}, 'incomes': {'before_savings': 250000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 320000}, {'debts': {}, 'incomes': {'before_savings': 320000, 'regular_income': 120000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 70000, 'after_savings': 390000}],
                }
            }
        }
    },
    "ex_si_more_c_credit": {
        "input_data": {
            "personal_data": {
                "current_savings": 2000000,
                "month_income": 100000,
                "month_rent": 50000,
                "month_expenses": 0,
                "deal_month_start": 5,
                "deal_month_finish": 12,
                "max_repairing_delay_months": 12,
                "inflation_percent": 0,
            },
            "credit_scheme": {
                "interest_rate": 3.2 / 1200,
                "months": 12,
            },
            "mortgage_schemes": {
                32342390239: {
                    "id": 32342390239,
                    "title": "12345678901234567890",
                    "initial_payment_percent": 15,
                    "initial_expenses": 60000,
                    "schedule": [
                        {
                            "interest_rate": 6.5 / 1200,
                            "months": 12,
                        },
                        {
                            "interest_rate": 10 / 1200,
                            "months": 228,
                        }
                    ]
                }
            },
            "realties": {
                83927978940983: {
                    "id": 83927978940983,
                    "title": "12345678901234567890",
                    "link": "https://yandex.ru",
                    "cost": 1800000,
                    "is_primary": True,
                    "get_keys_month": 14,
                    "repairing_expenses": 1000000,
                    "repairing_months": 3,
                    "area": None,
                    "subway_distance": None,
                    "has_mall": None,
                }
            }
        },
        "output_data": {83927978940983: {'no_mortgage': {'opt': {'credit': {'case': 'S_i > C, S < S_t', 'x': (5, 0), 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 2000000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 5}, {'cmd': 'spend_savings', 'payload': {'buy_realty': 1800000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 9}, {'cmd': 'get_credit', 'payload': {'name': 'Repairing', 'requested_sum': 100000, 'schedule': [{'interest_rate': 0.002666666666666667, 'months': 12}], 'minimal_initial_sum': 0, 'expenses': 0}}, {'cmd': 'spend_savings', 'payload': {'repairing': 1000000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 3}], 'result': {'months': 17, 'savings': 49598.220325923234, 'criteria': 16.50401779674077}}, 'savings': {'case': 'S_i > C, S > S_t', 'x': (5, 2), 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 2000000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 5}, {'cmd': 'spend_savings', 'payload': {'buy_realty': 1800000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 11}, {'cmd': 'spend_savings', 'payload': {'repairing': 1000000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 3}], 'result': {'months': 19, 'savings': 150000, 'criteria': 17.5}}}, 'combs': {(5, 0): {'credit': {'success': True, 'months': 17, 'savings': 49598.220325923234, 'criteria': 16.50401779674077}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (5, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (5, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 0): {'credit': {'success': True, 'months': 17, 'savings': 49598.220325923234, 'criteria': 16.50401779674077}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (6, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (6, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 0): {'credit': {'success': True, 'months': 17, 'savings': 49598.220325923234, 'criteria': 16.50401779674077}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (7, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (7, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 0): {'credit': {'success': True, 'months': 17, 'savings': 49598.220325923234, 'criteria': 16.50401779674077}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (8, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (8, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 0): {'credit': {'success': True, 'months': 17, 'savings': 49598.220325923234, 'criteria': 16.50401779674077}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (9, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (9, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 0): {'credit': {'success': True, 'months': 17, 'savings': 49598.220325923234, 'criteria': 16.50401779674077}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (10, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (10, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 0): {'credit': {'success': True, 'months': 17, 'savings': 49598.220325923234, 'criteria': 16.50401779674077}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (11, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (11, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}}}, 32342390239: {'opt': {'savings': {'case': 'S_i < C, S < S_t', 'x': (11, 0), 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 2000000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 11}, {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 163660.6290508546, 'schedule': [{'interest_rate': 0.005416666666666667, 'months': 12}, {'interest_rate': 0.008333333333333333, 'months': 228}], 'minimal_initial_sum': 270000.0, 'expenses': 60000}}, {'cmd': 'spend_savings', 'payload': {'buy_realty': 1800000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000, 'to_save': 48779.790316381805}, 'times': 3}, {'cmd': 'spend_savings', 'payload': {'repairing': 1000000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000}}], 'result': {'months': 18, 'savings': 85423.91486382134, 'criteria': 17.145760851361786}}}, 'combs': {(5, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 79603.84406673501, 'criteria': 17.20396155933265}}, (5, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 32483.46639519818, 'criteria': 17.675165336048018}}, (5, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 80656.13463033093, 'criteria': 17.193438653696692}}, (6, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 33222.894692551774, 'criteria': 17.667771053074482}}, (6, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 81635.77947508276, 'criteria': 17.18364220524917}}, (7, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 33902.10513356004, 'criteria': 17.6609789486644}}, (7, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 82602.08518112275, 'criteria': 17.17397914818877}}, (8, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 34571.98224902682, 'criteria': 17.654280177509733}}, (8, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 83555.35073034895, 'criteria': 17.16444649269651}}, (9, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 35232.738228931325, 'criteria': 17.647672617710686}}, (9, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 84495.86682304704, 'criteria': 17.155041331769528}}, (10, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 35884.57758246426, 'criteria': 17.641154224175356}}, (10, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 85423.91486382134, 'criteria': 17.145760851361786}}, (11, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 36527.6989738408, 'criteria': 17.63472301026159}}, (11, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}}}}},
        "expanded": {
            83927978940983: {
                'no_mortgage': {
                    'savings': [{'debts': {}, 'incomes': {'before_savings': 2000000, 'regular_income': 0, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0}, {'debts': {}, 'incomes': {'before_savings': 2000000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2050000}, {'debts': {}, 'incomes': {'before_savings': 2050000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2100000}, {'debts': {}, 'incomes': {'before_savings': 2100000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2150000}, {'debts': {}, 'incomes': {'before_savings': 2150000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2200000}, {'debts': {}, 'incomes': {'before_savings': 2200000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'buy_realty': 1800000}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 450000}, {'debts': {}, 'incomes': {'before_savings': 450000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 500000}, {'debts': {}, 'incomes': {'before_savings': 500000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 550000}, {'debts': {}, 'incomes': {'before_savings': 550000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 600000}, {'debts': {}, 'incomes': {'before_savings': 600000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 650000}, {'debts': {}, 'incomes': {'before_savings': 650000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 700000}, {'debts': {}, 'incomes': {'before_savings': 700000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 750000}, {'debts': {}, 'incomes': {'before_savings': 750000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 800000}, {'debts': {}, 'incomes': {'before_savings': 800000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 850000}, {'debts': {}, 'incomes': {'before_savings': 850000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 900000}, {'debts': {}, 'incomes': {'before_savings': 900000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 950000}, {'debts': {}, 'incomes': {'before_savings': 950000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'repairing': 1000000}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 0}, {'debts': {}, 'incomes': {'before_savings': 0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 50000}, {'debts': {}, 'incomes': {'before_savings': 50000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 100000}, {'debts': {}, 'incomes': {'before_savings': 100000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 150000}],
                    'credit': [{'debts': {}, 'incomes': {'before_savings': 2000000, 'regular_income': 0, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0}, {'debts': {}, 'incomes': {'before_savings': 2000000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2050000}, {'debts': {}, 'incomes': {'before_savings': 2050000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2100000}, {'debts': {}, 'incomes': {'before_savings': 2100000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2150000}, {'debts': {}, 'incomes': {'before_savings': 2150000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2200000}, {'debts': {}, 'incomes': {'before_savings': 2200000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'buy_realty': 1800000}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 450000}, {'debts': {}, 'incomes': {'before_savings': 450000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 500000}, {'debts': {}, 'incomes': {'before_savings': 500000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 550000}, {'debts': {}, 'incomes': {'before_savings': 550000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 600000}, {'debts': {}, 'incomes': {'before_savings': 600000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 650000}, {'debts': {}, 'incomes': {'before_savings': 650000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 700000}, {'debts': {}, 'incomes': {'before_savings': 700000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 750000}, {'debts': {}, 'incomes': {'before_savings': 750000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 800000}, {'debts': {}, 'incomes': {'before_savings': 800000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 850000}, {'debts': {'Repairing': {'interest_rate': 0.002666666666666667, 'before_debt': 0, 'before_minimal_payment': 0, 'before_closing_payment': 0, 'actual_payment': 0, 'after_debt': 100000, 'after_minimal_payment': 8478.482998637268, 'after_closing_payment': 100266.66666666667}}, 'incomes': {'before_savings': 850000, 'regular_income': 100000, 'irregular_income': 100000}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'mortgage_expenses': 0, 'repairing': 1000000}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 0}, {'debts': {'Repairing': {'interest_rate': 0.002666666666666667, 'before_debt': 100000, 'before_minimal_payment': 8478.482998637268, 'before_closing_payment': 100266.66666666667, 'actual_payment': 50000.0, 'after_debt': 50266.666666668, 'after_minimal_payment': 4643.1366402552785, 'after_closing_payment': 50400.711111112454}}, 'incomes': {'before_savings': 0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0}, {'debts': {'Repairing': {'interest_rate': 0.002666666666666667, 'before_debt': 50266.666666668, 'before_minimal_payment': 4643.1366402552785, 'before_closing_payment': 50400.711111112454, 'actual_payment': 50000.0, 'after_debt': 400.71111111379287, 'after_minimal_payment': 40.66116842261455, 'after_closing_payment': 401.779674076763}}, 'incomes': {'before_savings': 0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0}, {'debts': {'Repairing': {'interest_rate': 0.002666666666666667, 'before_debt': 400.71111111379287, 'before_minimal_payment': 40.66116842261455, 'before_closing_payment': 401.779674076763, 'actual_payment': 401.77967407676624, 'after_debt': 0, 'after_minimal_payment': 0, 'after_closing_payment': 0}}, 'incomes': {'before_savings': 0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 49598.220325923234, 'after_savings': 49598.220325923234}],
                },
                32342390239: {
                    'savings': [{'debts': {}, 'incomes': {'before_savings': 2000000, 'regular_income': 0, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0}, {'debts': {}, 'incomes': {'before_savings': 2000000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2050000}, {'debts': {}, 'incomes': {'before_savings': 2050000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2100000}, {'debts': {}, 'incomes': {'before_savings': 2100000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2150000}, {'debts': {}, 'incomes': {'before_savings': 2150000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2200000}, {'debts': {}, 'incomes': {'before_savings': 2200000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2250000}, {'debts': {}, 'incomes': {'before_savings': 2250000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2300000}, {'debts': {}, 'incomes': {'before_savings': 2300000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2350000}, {'debts': {}, 'incomes': {'before_savings': 2350000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2400000}, {'debts': {}, 'incomes': {'before_savings': 2400000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2450000}, {'debts': {}, 'incomes': {'before_savings': 2450000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2500000}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 0, 'before_minimal_payment': 0, 'before_closing_payment': 0, 'actual_payment': 0, 'after_debt': 163660.6290508546, 'after_minimal_payment': 1220.209683618193, 'after_closing_payment': 164547.12412488007}}, 'incomes': {'before_savings': 2500000, 'regular_income': 100000, 'irregular_income': 163660.6290508546}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'mortgage_expenses': 60000, 'buy_realty': 1800000}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 853660.6290508546}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 163660.6290508546, 'before_minimal_payment': 1220.209683618193, 'before_closing_payment': 164547.12412488007, 'actual_payment': 1220.209683618193, 'after_debt': 163326.91444126185, 'after_minimal_payment': 1220.209683618193, 'after_closing_payment': 164211.60189448536}}, 'incomes': {'before_savings': 853660.6290508546, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 48779.790316381805, 'last_save': 0, 'after_savings': 902440.4193672364}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 163326.91444126185, 'before_minimal_payment': 1220.209683618193, 'before_closing_payment': 164211.60189448536, 'actual_payment': 1220.209683618193, 'after_debt': 162991.39221086717, 'after_minimal_payment': 1220.209683618193, 'after_closing_payment': 163874.26225200936}}, 'incomes': {'before_savings': 902440.4193672364, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 48779.790316381805, 'last_save': 0, 'after_savings': 951220.2096836183}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 162991.39221086717, 'before_minimal_payment': 1220.209683618193, 'before_closing_payment': 163874.26225200936, 'actual_payment': 1220.209683618193, 'after_debt': 162654.05256839117, 'after_minimal_payment': 1220.2096836181931, 'after_closing_payment': 163535.09535313662}}, 'incomes': {'before_savings': 951220.2096836183, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'repairing': 1000000}}, 'to_save': 48779.790316381805, 'last_save': 0, 'after_savings': 1.1641532182693481e-10}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 162654.05256839117, 'before_minimal_payment': 1220.2096836181931, 'before_closing_payment': 163535.09535313662, 'actual_payment': 50000.0, 'after_debt': 113535.095353137, 'after_minimal_payment': 853.5053467768844, 'after_closing_payment': 114150.07711963316}}, 'incomes': {'before_savings': 1.1641532182693481e-10, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 1.1641532182693481e-10}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 113535.095353137, 'before_minimal_payment': 853.5053467768844, 'before_closing_payment': 114150.07711963316, 'actual_payment': 50000.0, 'after_debt': 64150.07711963355, 'after_minimal_payment': 483.26646571025685, 'after_closing_payment': 64497.556704031565}}, 'incomes': {'before_savings': 1.1641532182693481e-10, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 1.1641532182693481e-10}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 64150.07711963355, 'before_minimal_payment': 483.26646571025685, 'before_closing_payment': 64497.556704031565, 'actual_payment': 50000.0, 'after_debt': 14497.556704031944, 'after_minimal_payment': 109.44717130671863, 'after_closing_payment': 14576.085136178783}}, 'incomes': {'before_savings': 1.1641532182693481e-10, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 1.1641532182693481e-10}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 14497.556704031944, 'before_minimal_payment': 109.44717130671863, 'before_closing_payment': 14576.085136178783, 'actual_payment': 14576.085136178779, 'after_debt': 0, 'after_minimal_payment': 0, 'after_closing_payment': 0}}, 'incomes': {'before_savings': 1.1641532182693481e-10, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 85423.91486382122, 'after_savings': 85423.91486382134}],
                }
            }
        }
    },
    "ex_si_more_c_credit_infeasible": {
        "input_data": {
            "personal_data": {
                "current_savings": 2000000,
                "month_income": 100000,
                "month_rent": 50000,
                "month_expenses": 0,
                "deal_month_start": 0,
                "deal_month_finish": 12,
                "max_repairing_delay_months": 12,
                "inflation_percent": 0,
            },
            "credit_scheme": {
                "interest_rate": 3.2 / 1200,
                "months": 2,
            },
            "mortgage_schemes": {
                32342390239: {
                    "id": 32342390239,
                    "title": "12345678901234567890",
                    "initial_payment_percent": 15,
                    "initial_expenses": 60000,
                    "schedule": [
                        {
                            "interest_rate": 6.5 / 1200,
                            "months": 12,
                        },
                        {
                            "interest_rate": 10 / 1200,
                            "months": 228,
                        }
                    ]
                }
            },
            "realties": {
                83927978940983: {
                    "id": 83927978940983,
                    "title": "12345678901234567890",
                    "link": "https://yandex.ru",
                    "cost": 1800000,
                    "is_primary": True,
                    "get_keys_month": 14,
                    "repairing_expenses": 1000000,
                    "repairing_months": 3,
                    "area": None,
                    "subway_distance": None,
                    "has_mall": None,
                }
            }
        },
        "output_data": {83927978940983: {'no_mortgage': {'opt': {'credit': {'case': 'S_i > C, S < S_t', 'x': (0, 1), 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 2000000}}, {'cmd': 'spend_savings', 'payload': {'buy_realty': 1800000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 15}, {'cmd': 'get_credit', 'payload': {'name': 'Repairing', 'requested_sum': 50000, 'schedule': [{'interest_rate': 0.002666666666666667, 'months': 2}], 'minimal_initial_sum': 0, 'expenses': 0}}, {'cmd': 'spend_savings', 'payload': {'repairing': 1000000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 3}], 'result': {'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}}, 'savings': {'case': 'S_i > C, S > S_t', 'x': (0, 2), 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 2000000}}, {'cmd': 'spend_savings', 'payload': {'buy_realty': 1800000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 16}, {'cmd': 'spend_savings', 'payload': {'repairing': 1000000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 3}], 'result': {'months': 19, 'savings': 150000, 'criteria': 17.5}}}, 'combs': {(0, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50200.088770530085, 'have': 50000}, 'message': 'Credit MP: not enough money for minimal payment: need 50200 but have 50000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (0, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (0, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50200.088770530085, 'have': 50000}, 'message': 'Credit MP: not enough money for minimal payment: need 50200 but have 50000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (1, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (1, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50200.088770530085, 'have': 50000}, 'message': 'Credit MP: not enough money for minimal payment: need 50200 but have 50000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (2, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (2, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50200.088770530085, 'have': 50000}, 'message': 'Credit MP: not enough money for minimal payment: need 50200 but have 50000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (3, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (3, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50200.088770530085, 'have': 50000}, 'message': 'Credit MP: not enough money for minimal payment: need 50200 but have 50000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (4, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (4, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50200.088770530085, 'have': 50000}, 'message': 'Credit MP: not enough money for minimal payment: need 50200 but have 50000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (5, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (5, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50200.088770530085, 'have': 50000}, 'message': 'Credit MP: not enough money for minimal payment: need 50200 but have 50000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (6, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (6, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50200.088770530085, 'have': 50000}, 'message': 'Credit MP: not enough money for minimal payment: need 50200 but have 50000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (7, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (7, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50200.088770530085, 'have': 50000}, 'message': 'Credit MP: not enough money for minimal payment: need 50200 but have 50000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (8, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (8, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50200.088770530085, 'have': 50000}, 'message': 'Credit MP: not enough money for minimal payment: need 50200 but have 50000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (9, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (9, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50200.088770530085, 'have': 50000}, 'message': 'Credit MP: not enough money for minimal payment: need 50200 but have 50000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (10, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (10, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50200.088770530085, 'have': 50000}, 'message': 'Credit MP: not enough money for minimal payment: need 50200 but have 50000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 900000, 'need': 1000000}}}, (11, 1): {'credit': {'success': True, 'months': 18, 'savings': 99866.31111110977, 'criteria': 17.001336888888904}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 950000, 'need': 1000000}}}, (11, 2): {'savings': {'success': True, 'months': 19, 'savings': 150000, 'criteria': 17.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 3): {'savings': {'success': True, 'months': 20, 'savings': 200000, 'criteria': 18.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 4): {'savings': {'success': True, 'months': 21, 'savings': 250000, 'criteria': 18.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 5): {'savings': {'success': True, 'months': 22, 'savings': 300000, 'criteria': 19.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 6): {'savings': {'success': True, 'months': 23, 'savings': 350000, 'criteria': 19.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 7): {'savings': {'success': True, 'months': 24, 'savings': 400000, 'criteria': 20.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 8): {'savings': {'success': True, 'months': 25, 'savings': 450000, 'criteria': 20.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 9): {'savings': {'success': True, 'months': 26, 'savings': 500000, 'criteria': 21.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 10): {'savings': {'success': True, 'months': 27, 'savings': 550000, 'criteria': 21.5}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 11): {'savings': {'success': True, 'months': 28, 'savings': 600000, 'criteria': 22.0}, 'credit': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}}}, 32342390239: {'opt': {'savings': {'case': 'S_i < C, S < S_t', 'x': (11, 0), 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 2000000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 11}, {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 163660.6290508546, 'schedule': [{'interest_rate': 0.005416666666666667, 'months': 12}, {'interest_rate': 0.008333333333333333, 'months': 228}], 'minimal_initial_sum': 270000.0, 'expenses': 60000}}, {'cmd': 'spend_savings', 'payload': {'buy_realty': 1800000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000, 'to_save': 48779.790316381805}, 'times': 3}, {'cmd': 'spend_savings', 'payload': {'repairing': 1000000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 50000}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000}}], 'result': {'months': 18, 'savings': 85423.91486382134, 'criteria': 17.145760851361786}}}, 'combs': {(0, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 72178.13757812396, 'criteria': 17.27821862421876}}, (0, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 27151.052626643464, 'criteria': 17.728489473733564}}, (0, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (0, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 73825.76921923527, 'criteria': 17.261742307807648}}, (1, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 28297.258418164904, 'criteria': 17.71702741581835}}, (1, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (1, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 75440.96597431455, 'criteria': 17.245590340256854}}, (2, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 29420.643876420916, 'criteria': 17.70579356123579}}, (2, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (2, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 76998.39798892109, 'criteria': 17.23001602011079}}, (3, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 30521.91414284039, 'criteria': 17.694780858571598}}, (3, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (3, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 78385.30626211477, 'criteria': 17.216146937378852}}, (4, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 31583.799675587925, 'criteria': 17.684162003244122}}, (4, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (4, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 79603.84406673501, 'criteria': 17.20396155933265}}, (5, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 32483.46639519818, 'criteria': 17.675165336048018}}, (5, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (5, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 80656.13463033093, 'criteria': 17.193438653696692}}, (6, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 33222.894692551774, 'criteria': 17.667771053074482}}, (6, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (6, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 81635.77947508276, 'criteria': 17.18364220524917}}, (7, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 33902.10513356004, 'criteria': 17.6609789486644}}, (7, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (7, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 82602.08518112275, 'criteria': 17.17397914818877}}, (8, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 34571.98224902682, 'criteria': 17.654280177509733}}, (8, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (8, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 83555.35073034895, 'criteria': 17.16444649269651}}, (9, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 35232.738228931325, 'criteria': 17.647672617710686}}, (9, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (9, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 84495.86682304704, 'criteria': 17.155041331769528}}, (10, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 35884.57758246426, 'criteria': 17.641154224175356}}, (10, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (10, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 85423.91486382134, 'criteria': 17.145760851361786}}, (11, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 18, 'savings': 36527.6989738408, 'criteria': 17.63472301026159}}, (11, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}, (11, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': False, 'code': 'S_i > C, S > S_t', 'message': 'Not needed'}}}}}},
        "expanded": {
            83927978940983: {
                'no_mortgage': {
                    'savings': [{'debts': {}, 'incomes': {'before_savings': 2000000, 'regular_income': 0, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {'buy_realty': 1800000}}, 'to_save': 0, 'last_save': 0, 'after_savings': -1800000}, {'debts': {}, 'incomes': {'before_savings': 200000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 250000}, {'debts': {}, 'incomes': {'before_savings': 250000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 300000}, {'debts': {}, 'incomes': {'before_savings': 300000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 350000}, {'debts': {}, 'incomes': {'before_savings': 350000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 400000}, {'debts': {}, 'incomes': {'before_savings': 400000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 450000}, {'debts': {}, 'incomes': {'before_savings': 450000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 500000}, {'debts': {}, 'incomes': {'before_savings': 500000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 550000}, {'debts': {}, 'incomes': {'before_savings': 550000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 600000}, {'debts': {}, 'incomes': {'before_savings': 600000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 650000}, {'debts': {}, 'incomes': {'before_savings': 650000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 700000}, {'debts': {}, 'incomes': {'before_savings': 700000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 750000}, {'debts': {}, 'incomes': {'before_savings': 750000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 800000}, {'debts': {}, 'incomes': {'before_savings': 800000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 850000}, {'debts': {}, 'incomes': {'before_savings': 850000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 900000}, {'debts': {}, 'incomes': {'before_savings': 900000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 950000}, {'debts': {}, 'incomes': {'before_savings': 950000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'repairing': 1000000}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 0}, {'debts': {}, 'incomes': {'before_savings': 0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 50000}, {'debts': {}, 'incomes': {'before_savings': 50000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 100000}, {'debts': {}, 'incomes': {'before_savings': 100000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 150000}],
                    'credit': [{'debts': {}, 'incomes': {'before_savings': 2000000, 'regular_income': 0, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {'buy_realty': 1800000}}, 'to_save': 0, 'last_save': 0, 'after_savings': -1800000}, {'debts': {}, 'incomes': {'before_savings': 200000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 250000}, {'debts': {}, 'incomes': {'before_savings': 250000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 300000}, {'debts': {}, 'incomes': {'before_savings': 300000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 350000}, {'debts': {}, 'incomes': {'before_savings': 350000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 400000}, {'debts': {}, 'incomes': {'before_savings': 400000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 450000}, {'debts': {}, 'incomes': {'before_savings': 450000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 500000}, {'debts': {}, 'incomes': {'before_savings': 500000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 550000}, {'debts': {}, 'incomes': {'before_savings': 550000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 600000}, {'debts': {}, 'incomes': {'before_savings': 600000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 650000}, {'debts': {}, 'incomes': {'before_savings': 650000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 700000}, {'debts': {}, 'incomes': {'before_savings': 700000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 750000}, {'debts': {}, 'incomes': {'before_savings': 750000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 800000}, {'debts': {}, 'incomes': {'before_savings': 800000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 850000}, {'debts': {}, 'incomes': {'before_savings': 850000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 900000}, {'debts': {'Repairing': {'interest_rate': 0.002666666666666667, 'before_debt': 0, 'before_minimal_payment': 0, 'before_closing_payment': 0, 'actual_payment': 0, 'after_debt': 50000, 'after_minimal_payment': 25100.044385265042, 'after_closing_payment': 50133.333333333336}}, 'incomes': {'before_savings': 900000, 'regular_income': 100000, 'irregular_income': 50000}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'mortgage_expenses': 0, 'repairing': 1000000}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 0}, {'debts': {'Repairing': {'interest_rate': 0.002666666666666667, 'before_debt': 50000, 'before_minimal_payment': 25100.044385265042, 'before_closing_payment': 50133.333333333336, 'actual_payment': 50000.0, 'after_debt': 133.33333333467453, 'after_minimal_payment': 133.6888888902356, 'after_closing_payment': 133.68888889023367}}, 'incomes': {'before_savings': 0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0}, {'debts': {'Repairing': {'interest_rate': 0.002666666666666667, 'before_debt': 133.33333333467453, 'before_minimal_payment': 133.6888888902356, 'before_closing_payment': 133.68888889023367, 'actual_payment': 133.6888888902322, 'after_debt': 0, 'after_minimal_payment': 0, 'after_closing_payment': 0}}, 'incomes': {'before_savings': 0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 49866.31111110977, 'after_savings': 49866.31111110977}, {'debts': {}, 'incomes': {'before_savings': 49866.31111110977, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 99866.31111110977}],
                },
                32342390239: {
                    'savings': [{'debts': {}, 'incomes': {'before_savings': 2000000, 'regular_income': 0, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0}, {'debts': {}, 'incomes': {'before_savings': 2000000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2050000}, {'debts': {}, 'incomes': {'before_savings': 2050000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2100000}, {'debts': {}, 'incomes': {'before_savings': 2100000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2150000}, {'debts': {}, 'incomes': {'before_savings': 2150000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2200000}, {'debts': {}, 'incomes': {'before_savings': 2200000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2250000}, {'debts': {}, 'incomes': {'before_savings': 2250000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2300000}, {'debts': {}, 'incomes': {'before_savings': 2300000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2350000}, {'debts': {}, 'incomes': {'before_savings': 2350000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2400000}, {'debts': {}, 'incomes': {'before_savings': 2400000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2450000}, {'debts': {}, 'incomes': {'before_savings': 2450000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 2500000}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 0, 'before_minimal_payment': 0, 'before_closing_payment': 0, 'actual_payment': 0, 'after_debt': 163660.6290508546, 'after_minimal_payment': 1220.209683618193, 'after_closing_payment': 164547.12412488007}}, 'incomes': {'before_savings': 2500000, 'regular_income': 100000, 'irregular_income': 163660.6290508546}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'mortgage_expenses': 60000, 'buy_realty': 1800000}}, 'to_save': 0, 'last_save': 50000, 'after_savings': 853660.6290508546}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 163660.6290508546, 'before_minimal_payment': 1220.209683618193, 'before_closing_payment': 164547.12412488007, 'actual_payment': 1220.209683618193, 'after_debt': 163326.91444126185, 'after_minimal_payment': 1220.209683618193, 'after_closing_payment': 164211.60189448536}}, 'incomes': {'before_savings': 853660.6290508546, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 48779.790316381805, 'last_save': 0, 'after_savings': 902440.4193672364}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 163326.91444126185, 'before_minimal_payment': 1220.209683618193, 'before_closing_payment': 164211.60189448536, 'actual_payment': 1220.209683618193, 'after_debt': 162991.39221086717, 'after_minimal_payment': 1220.209683618193, 'after_closing_payment': 163874.26225200936}}, 'incomes': {'before_savings': 902440.4193672364, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 48779.790316381805, 'last_save': 0, 'after_savings': 951220.2096836183}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 162991.39221086717, 'before_minimal_payment': 1220.209683618193, 'before_closing_payment': 163874.26225200936, 'actual_payment': 1220.209683618193, 'after_debt': 162654.05256839117, 'after_minimal_payment': 1220.2096836181931, 'after_closing_payment': 163535.09535313662}}, 'incomes': {'before_savings': 951220.2096836183, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'repairing': 1000000}}, 'to_save': 48779.790316381805, 'last_save': 0, 'after_savings': 1.1641532182693481e-10}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 162654.05256839117, 'before_minimal_payment': 1220.2096836181931, 'before_closing_payment': 163535.09535313662, 'actual_payment': 50000.0, 'after_debt': 113535.095353137, 'after_minimal_payment': 853.5053467768844, 'after_closing_payment': 114150.07711963316}}, 'incomes': {'before_savings': 1.1641532182693481e-10, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 1.1641532182693481e-10}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 113535.095353137, 'before_minimal_payment': 853.5053467768844, 'before_closing_payment': 114150.07711963316, 'actual_payment': 50000.0, 'after_debt': 64150.07711963355, 'after_minimal_payment': 483.26646571025685, 'after_closing_payment': 64497.556704031565}}, 'incomes': {'before_savings': 1.1641532182693481e-10, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 1.1641532182693481e-10}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 64150.07711963355, 'before_minimal_payment': 483.26646571025685, 'before_closing_payment': 64497.556704031565, 'actual_payment': 50000.0, 'after_debt': 14497.556704031944, 'after_minimal_payment': 109.44717130671863, 'after_closing_payment': 14576.085136178783}}, 'incomes': {'before_savings': 1.1641532182693481e-10, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 1.1641532182693481e-10}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 14497.556704031944, 'before_minimal_payment': 109.44717130671863, 'before_closing_payment': 14576.085136178783, 'actual_payment': 14576.085136178779, 'after_debt': 0, 'after_minimal_payment': 0, 'after_closing_payment': 0}}, 'incomes': {'before_savings': 1.1641532182693481e-10, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 85423.91486382122, 'after_savings': 85423.91486382134}],
                }
            }
        }
    },
    "ex_si_more_c_no_credit": {
        "input_data": {
            "personal_data": {
                "current_savings": 2000000,
                "month_income": 60000,
                "month_rent": 50000,
                "month_expenses": 0,
                "deal_month_start": 0,
                "deal_month_finish": 12,
                "max_repairing_delay_months": 12,
                "inflation_percent": 0,
            },
            "credit_scheme": {
                "interest_rate": 3.2 / 1200,
                "months": 12,
            },
            "mortgage_schemes": {
                32342390239: {
                    "id": 32342390239,
                    "title": "12345678901234567890",
                    "initial_payment_percent": 15,
                    "initial_expenses": 60000,
                    "schedule": [
                        {
                            "interest_rate": 6.5 / 1200,
                            "months": 12,
                        },
                        {
                            "interest_rate": 10 / 1200,
                            "months": 228,
                        }
                    ]
                }
            },
            "realties": {
                83927978940983: {
                    "id": 83927978940983,
                    "title": "12345678901234567890",
                    "link": "https://yandex.ru",
                    "cost": 1800000,
                    "is_primary": True,
                    "get_keys_month": 14,
                    "repairing_expenses": 1000000,
                    "repairing_months": 3,
                    "area": None,
                    "subway_distance": None,
                    "has_mall": None,
                }
            }
        },
        "output_data": {83927978940983: {'no_mortgage': {'opt': None, 'combs': {(0, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55957.987791005966, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55957 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 340000, 'need': 1000000}}}, (0, 1): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55110.13949114224, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55110 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 350000, 'need': 1000000}}}, (0, 2): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 54262.29119127852, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 54262 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 360000, 'need': 1000000}}}, (0, 3): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 53414.44289141479, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 53414 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 370000, 'need': 1000000}}}, (0, 4): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 52566.59459155106, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 52566 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 380000, 'need': 1000000}}}, (0, 5): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 51718.74629168734, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 51718 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 390000, 'need': 1000000}}}, (0, 6): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50870.89799182361, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50870 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 400000, 'need': 1000000}}}, (0, 7): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50023.04969195988, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50023 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 410000, 'need': 1000000}}}, (0, 8): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 49175.201392096154, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 49175 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 420000, 'need': 1000000}}}, (0, 9): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 48327.35309223243, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 48327 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 430000, 'need': 1000000}}}, (0, 10): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 47479.5047923687, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 47479 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 440000, 'need': 1000000}}}, (0, 11): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 46631.656492504975, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 46631 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 450000, 'need': 1000000}}}, (1, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55957.987791005966, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55957 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 340000, 'need': 1000000}}}, (1, 1): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55110.13949114224, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55110 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 350000, 'need': 1000000}}}, (1, 2): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 54262.29119127852, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 54262 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 360000, 'need': 1000000}}}, (1, 3): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 53414.44289141479, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 53414 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 370000, 'need': 1000000}}}, (1, 4): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 52566.59459155106, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 52566 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 380000, 'need': 1000000}}}, (1, 5): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 51718.74629168734, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 51718 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 390000, 'need': 1000000}}}, (1, 6): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50870.89799182361, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50870 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 400000, 'need': 1000000}}}, (1, 7): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50023.04969195988, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50023 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 410000, 'need': 1000000}}}, (1, 8): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 49175.201392096154, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 49175 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 420000, 'need': 1000000}}}, (1, 9): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 48327.35309223243, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 48327 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 430000, 'need': 1000000}}}, (1, 10): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 47479.5047923687, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 47479 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 440000, 'need': 1000000}}}, (1, 11): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 46631.656492504975, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 46631 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 450000, 'need': 1000000}}}, (2, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55957.987791005966, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55957 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 340000, 'need': 1000000}}}, (2, 1): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55110.13949114224, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55110 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 350000, 'need': 1000000}}}, (2, 2): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 54262.29119127852, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 54262 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 360000, 'need': 1000000}}}, (2, 3): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 53414.44289141479, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 53414 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 370000, 'need': 1000000}}}, (2, 4): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 52566.59459155106, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 52566 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 380000, 'need': 1000000}}}, (2, 5): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 51718.74629168734, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 51718 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 390000, 'need': 1000000}}}, (2, 6): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50870.89799182361, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50870 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 400000, 'need': 1000000}}}, (2, 7): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50023.04969195988, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50023 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 410000, 'need': 1000000}}}, (2, 8): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 49175.201392096154, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 49175 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 420000, 'need': 1000000}}}, (2, 9): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 48327.35309223243, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 48327 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 430000, 'need': 1000000}}}, (2, 10): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 47479.5047923687, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 47479 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 440000, 'need': 1000000}}}, (2, 11): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 46631.656492504975, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 46631 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 450000, 'need': 1000000}}}, (3, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55957.987791005966, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55957 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 340000, 'need': 1000000}}}, (3, 1): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55110.13949114224, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55110 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 350000, 'need': 1000000}}}, (3, 2): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 54262.29119127852, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 54262 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 360000, 'need': 1000000}}}, (3, 3): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 53414.44289141479, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 53414 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 370000, 'need': 1000000}}}, (3, 4): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 52566.59459155106, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 52566 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 380000, 'need': 1000000}}}, (3, 5): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 51718.74629168734, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 51718 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 390000, 'need': 1000000}}}, (3, 6): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50870.89799182361, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50870 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 400000, 'need': 1000000}}}, (3, 7): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50023.04969195988, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50023 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 410000, 'need': 1000000}}}, (3, 8): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 49175.201392096154, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 49175 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 420000, 'need': 1000000}}}, (3, 9): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 48327.35309223243, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 48327 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 430000, 'need': 1000000}}}, (3, 10): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 47479.5047923687, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 47479 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 440000, 'need': 1000000}}}, (3, 11): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 46631.656492504975, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 46631 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 450000, 'need': 1000000}}}, (4, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55957.987791005966, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55957 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 340000, 'need': 1000000}}}, (4, 1): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55110.13949114224, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55110 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 350000, 'need': 1000000}}}, (4, 2): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 54262.29119127852, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 54262 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 360000, 'need': 1000000}}}, (4, 3): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 53414.44289141479, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 53414 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 370000, 'need': 1000000}}}, (4, 4): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 52566.59459155106, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 52566 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 380000, 'need': 1000000}}}, (4, 5): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 51718.74629168734, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 51718 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 390000, 'need': 1000000}}}, (4, 6): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50870.89799182361, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50870 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 400000, 'need': 1000000}}}, (4, 7): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50023.04969195988, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50023 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 410000, 'need': 1000000}}}, (4, 8): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 49175.201392096154, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 49175 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 420000, 'need': 1000000}}}, (4, 9): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 48327.35309223243, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 48327 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 430000, 'need': 1000000}}}, (4, 10): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 47479.5047923687, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 47479 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 440000, 'need': 1000000}}}, (4, 11): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 46631.656492504975, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 46631 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 450000, 'need': 1000000}}}, (5, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55957.987791005966, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55957 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 340000, 'need': 1000000}}}, (5, 1): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55110.13949114224, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55110 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 350000, 'need': 1000000}}}, (5, 2): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 54262.29119127852, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 54262 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 360000, 'need': 1000000}}}, (5, 3): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 53414.44289141479, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 53414 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 370000, 'need': 1000000}}}, (5, 4): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 52566.59459155106, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 52566 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 380000, 'need': 1000000}}}, (5, 5): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 51718.74629168734, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 51718 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 390000, 'need': 1000000}}}, (5, 6): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50870.89799182361, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50870 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 400000, 'need': 1000000}}}, (5, 7): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50023.04969195988, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50023 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 410000, 'need': 1000000}}}, (5, 8): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 49175.201392096154, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 49175 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 420000, 'need': 1000000}}}, (5, 9): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 48327.35309223243, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 48327 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 430000, 'need': 1000000}}}, (5, 10): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 47479.5047923687, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 47479 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 440000, 'need': 1000000}}}, (5, 11): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 46631.656492504975, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 46631 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 450000, 'need': 1000000}}}, (6, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55957.987791005966, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55957 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 340000, 'need': 1000000}}}, (6, 1): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55110.13949114224, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55110 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 350000, 'need': 1000000}}}, (6, 2): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 54262.29119127852, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 54262 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 360000, 'need': 1000000}}}, (6, 3): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 53414.44289141479, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 53414 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 370000, 'need': 1000000}}}, (6, 4): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 52566.59459155106, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 52566 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 380000, 'need': 1000000}}}, (6, 5): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 51718.74629168734, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 51718 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 390000, 'need': 1000000}}}, (6, 6): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50870.89799182361, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50870 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 400000, 'need': 1000000}}}, (6, 7): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50023.04969195988, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50023 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 410000, 'need': 1000000}}}, (6, 8): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 49175.201392096154, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 49175 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 420000, 'need': 1000000}}}, (6, 9): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 48327.35309223243, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 48327 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 430000, 'need': 1000000}}}, (6, 10): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 47479.5047923687, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 47479 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 440000, 'need': 1000000}}}, (6, 11): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 46631.656492504975, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 46631 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 450000, 'need': 1000000}}}, (7, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55957.987791005966, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55957 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 340000, 'need': 1000000}}}, (7, 1): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55110.13949114224, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55110 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 350000, 'need': 1000000}}}, (7, 2): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 54262.29119127852, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 54262 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 360000, 'need': 1000000}}}, (7, 3): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 53414.44289141479, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 53414 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 370000, 'need': 1000000}}}, (7, 4): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 52566.59459155106, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 52566 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 380000, 'need': 1000000}}}, (7, 5): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 51718.74629168734, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 51718 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 390000, 'need': 1000000}}}, (7, 6): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50870.89799182361, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50870 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 400000, 'need': 1000000}}}, (7, 7): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50023.04969195988, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50023 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 410000, 'need': 1000000}}}, (7, 8): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 49175.201392096154, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 49175 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 420000, 'need': 1000000}}}, (7, 9): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 48327.35309223243, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 48327 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 430000, 'need': 1000000}}}, (7, 10): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 47479.5047923687, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 47479 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 440000, 'need': 1000000}}}, (7, 11): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 46631.656492504975, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 46631 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 450000, 'need': 1000000}}}, (8, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55957.987791005966, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55957 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 340000, 'need': 1000000}}}, (8, 1): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55110.13949114224, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55110 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 350000, 'need': 1000000}}}, (8, 2): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 54262.29119127852, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 54262 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 360000, 'need': 1000000}}}, (8, 3): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 53414.44289141479, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 53414 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 370000, 'need': 1000000}}}, (8, 4): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 52566.59459155106, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 52566 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 380000, 'need': 1000000}}}, (8, 5): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 51718.74629168734, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 51718 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 390000, 'need': 1000000}}}, (8, 6): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50870.89799182361, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50870 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 400000, 'need': 1000000}}}, (8, 7): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50023.04969195988, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50023 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 410000, 'need': 1000000}}}, (8, 8): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 49175.201392096154, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 49175 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 420000, 'need': 1000000}}}, (8, 9): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 48327.35309223243, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 48327 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 430000, 'need': 1000000}}}, (8, 10): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 47479.5047923687, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 47479 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 440000, 'need': 1000000}}}, (8, 11): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 46631.656492504975, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 46631 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 450000, 'need': 1000000}}}, (9, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55957.987791005966, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55957 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 340000, 'need': 1000000}}}, (9, 1): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55110.13949114224, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55110 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 350000, 'need': 1000000}}}, (9, 2): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 54262.29119127852, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 54262 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 360000, 'need': 1000000}}}, (9, 3): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 53414.44289141479, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 53414 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 370000, 'need': 1000000}}}, (9, 4): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 52566.59459155106, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 52566 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 380000, 'need': 1000000}}}, (9, 5): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 51718.74629168734, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 51718 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 390000, 'need': 1000000}}}, (9, 6): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50870.89799182361, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50870 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 400000, 'need': 1000000}}}, (9, 7): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50023.04969195988, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50023 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 410000, 'need': 1000000}}}, (9, 8): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 49175.201392096154, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 49175 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 420000, 'need': 1000000}}}, (9, 9): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 48327.35309223243, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 48327 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 430000, 'need': 1000000}}}, (9, 10): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 47479.5047923687, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 47479 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 440000, 'need': 1000000}}}, (9, 11): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 46631.656492504975, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 46631 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 450000, 'need': 1000000}}}, (10, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55957.987791005966, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55957 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 340000, 'need': 1000000}}}, (10, 1): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55110.13949114224, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55110 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 350000, 'need': 1000000}}}, (10, 2): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 54262.29119127852, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 54262 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 360000, 'need': 1000000}}}, (10, 3): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 53414.44289141479, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 53414 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 370000, 'need': 1000000}}}, (10, 4): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 52566.59459155106, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 52566 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 380000, 'need': 1000000}}}, (10, 5): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 51718.74629168734, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 51718 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 390000, 'need': 1000000}}}, (10, 6): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50870.89799182361, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50870 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 400000, 'need': 1000000}}}, (10, 7): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50023.04969195988, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50023 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 410000, 'need': 1000000}}}, (10, 8): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 49175.201392096154, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 49175 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 420000, 'need': 1000000}}}, (10, 9): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 48327.35309223243, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 48327 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 430000, 'need': 1000000}}}, (10, 10): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 47479.5047923687, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 47479 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 440000, 'need': 1000000}}}, (10, 11): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 46631.656492504975, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 46631 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 450000, 'need': 1000000}}}, (11, 0): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55957.987791005966, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55957 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 340000, 'need': 1000000}}}, (11, 1): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 55110.13949114224, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 55110 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 350000, 'need': 1000000}}}, (11, 2): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 54262.29119127852, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 54262 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 360000, 'need': 1000000}}}, (11, 3): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 53414.44289141479, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 53414 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 370000, 'need': 1000000}}}, (11, 4): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 52566.59459155106, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 52566 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 380000, 'need': 1000000}}}, (11, 5): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 51718.74629168734, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 51718 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 390000, 'need': 1000000}}}, (11, 6): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50870.89799182361, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50870 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 400000, 'need': 1000000}}}, (11, 7): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 50023.04969195988, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 50023 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 410000, 'need': 1000000}}}, (11, 8): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 49175.201392096154, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 49175 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 420000, 'need': 1000000}}}, (11, 9): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 48327.35309223243, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 48327 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 430000, 'need': 1000000}}}, (11, 10): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 47479.5047923687, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 47479 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 440000, 'need': 1000000}}}, (11, 11): {'credit': {'success': False, 'code': 'CRD_MP', 'palyload': {'need': 46631.656492504975, 'have': 10000}, 'message': 'Credit MP: not enough money for minimal payment: need 46631 but have 10000'}, 'savings': {'success': False, 'code': 'S_i > C, S < S_t', 'payload': {'have': 450000, 'need': 1000000}}}}}, 32342390239: {'opt': {'savings': {'case': 'S_i < C, S < S_t', 'x': (11, 0), 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 2000000}}, {'cmd': 'make_month', 'payload': {'earned': 60000, 'month_rent': 50000}, 'times': 11}, {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 736472.83, 'schedule': [{'interest_rate': 0.005416666666666667, 'months': 12}, {'interest_rate': 0.008333333333333333, 'months': 228}], 'minimal_initial_sum': 270000.0, 'expenses': 60000}}, {'cmd': 'spend_savings', 'payload': {'buy_realty': 1800000}}, {'cmd': 'make_month', 'payload': {'earned': 60000, 'month_rent': 50000, 'to_save': 4509.0564}, 'times': 3}, {'cmd': 'spend_savings', 'payload': {'repairing': 1000000}}, {'cmd': 'make_month', 'payload': {'earned': 60000, 'month_rent': 50000}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 60000}, 'times': 13}], 'result': {'months': 30, 'savings': 36034.89483102936, 'criteria': 29.399418419482846}}}, 'combs': {(0, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 31, 'savings': 17072.098836371908, 'criteria': 30.7154650193938}}, (0, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 32, 'savings': 21317.351283016884, 'criteria': 31.644710811949718}}, (0, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 33, 'savings': 25628.29026791268, 'criteria': 32.57286182886812}}, (0, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 34, 'savings': 30006.70426807246, 'criteria': 33.499888262198795}}, (0, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 35, 'savings': 34454.46065806253, 'criteria': 34.42575898903229}}, (0, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 36, 'savings': 38973.503912785214, 'criteria': 35.35044160145358}}, (0, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 37, 'savings': 43565.86074757099, 'criteria': 36.273902320873816}}, (0, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 38, 'savings': 48233.65213683845, 'criteria': 37.19610579771936}}, (0, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 39, 'savings': 52979.08943249044, 'criteria': 38.11701517612516}}, (0, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 40, 'savings': 57804.48309759736, 'criteria': 39.03659194837338}}, (0, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 40, 'savings': 2689.838150806405, 'criteria': 39.95516936415323}}, (0, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 41, 'savings': 7641.254306319257, 'criteria': 40.872645761561344}}, (1, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 31, 'savings': 25331.074032293633, 'criteria': 30.577815432795106}}, (1, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 32, 'savings': 29627.064695098434, 'criteria': 31.506215588415024}}, (1, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 33, 'savings': 33989.35489026022, 'criteria': 32.433510751829}}, (1, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 34, 'savings': 38419.73710822873, 'criteria': 33.35967104819619}}, (1, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 35, 'savings': 42920.08094070722, 'criteria': 34.28466531765488}}, (1, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 36, 'savings': 47492.33191022259, 'criteria': 35.208461134829626}}, (1, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 37, 'savings': 52138.52012012024, 'criteria': 36.131024664664665}}, (1, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 38, 'savings': 56860.76655810517, 'criteria': 37.052320557364915}}, (1, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 38, 'savings': 1647.5528944770194, 'criteria': 37.97254078509205}}, (1, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 39, 'savings': 6488.308854595416, 'criteria': 38.89186151909008}}, (1, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 40, 'savings': 11411.375844465925, 'criteria': 39.809810402592234}}, (1, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 41, 'savings': 16419.255168070464, 'criteria': 40.72634574719883}}, (2, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 31, 'savings': 33427.46902736822, 'criteria': 30.44287551621053}}, (2, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 32, 'savings': 37771.3319021878, 'criteria': 31.370477801630205}}, (2, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 33, 'savings': 42182.03055382389, 'criteria': 32.296966157436266}}, (2, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 34, 'savings': 46661.35873986158, 'criteria': 33.22231068766897}}, (2, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 35, 'savings': 51211.186132146635, 'criteria': 34.14648023113089}}, (2, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 36, 'savings': 55833.45604768508, 'criteria': 35.06944239920525}}, (2, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 36, 'savings': 525.8194180116479, 'criteria': 35.99123634303314}}, (2, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 37, 'savings': 5259.706212960773, 'criteria': 36.912338229783984}}, (2, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 38, 'savings': 10071.74122023816, 'criteria': 37.832137646329365}}, (2, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 39, 'savings': 14964.211120315558, 'criteria': 38.750596481328074}}, (2, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 40, 'savings': 19939.505431842845, 'criteria': 39.667674909469284}}, (2, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 41, 'savings': 25000.12172250743, 'criteria': 40.58333130462488}}, (3, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 31, 'savings': 41234.307669551956, 'criteria': 30.3127615388408}}, (3, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 32, 'savings': 45755.27702895489, 'criteria': 31.237412049517417}}, (3, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 33, 'savings': 50211.5898769327, 'criteria': 32.16314016871779}}, (3, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 34, 'savings': 54736.99680546239, 'criteria': 33.08771671990896}}, (3, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 35, 'savings': 59333.36282572952, 'criteria': 34.01111061957118}}, (3, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 35, 'savings': 3969.5532530454075, 'criteria': 34.93384077911591}}, (3, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 36, 'savings': 8674.544811976775, 'criteria': 35.85542425313372}}, (3, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 37, 'savings': 13455.939359699543, 'criteria': 36.77573434400501}}, (3, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 38, 'savings': 18315.922138696747, 'criteria': 37.694734631021724}}, (3, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 39, 'savings': 23256.771805752687, 'criteria': 38.61238713657079}}, (3, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 40, 'savings': 28280.86951204471, 'criteria': 39.528652174799255}}, (3, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 41, 'savings': 33390.70230536862, 'criteria': 40.44348829491052}}, (4, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 31, 'savings': 48896.583377494244, 'criteria': 30.185056943708428}}, (4, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 32, 'savings': 53453.68712781517, 'criteria': 31.109105214536413}}, (4, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 33, 'savings': 58083.085009845665, 'criteria': 32.03194858316924}}, (4, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 33, 'savings': 2629.932117262273, 'criteria': 32.95616779804563}}, (4, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 34, 'savings': 7231.698944792042, 'criteria': 33.87947168425347}}, (4, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 35, 'savings': 11906.150134179887, 'criteria': 34.80156416443034}}, (4, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 36, 'savings': 16655.28668540567, 'criteria': 35.72241188857657}}, (4, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 37, 'savings': 21481.19784525544, 'criteria': 36.64198003591241}}, (4, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 38, 'savings': 26386.058018108517, 'criteria': 37.56023236636486}}, (4, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 39, 'savings': 31372.136582444316, 'criteria': 38.47713105695926}}, (4, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 40, 'savings': 36441.80238485395, 'criteria': 39.3926366269191}}, (4, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 41, 'savings': 41597.52795896656, 'criteria': 40.30670786735056}}, (5, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 31, 'savings': 56417.16662524278, 'criteria': 30.059713889579285}}, (5, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 31, 'savings': 1000.7442895850618, 'criteria': 30.983320928506917}}, (5, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 32, 'savings': 5626.181905879748, 'criteria': 31.90623030156867}}, (5, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 33, 'savings': 10324.852547024057, 'criteria': 32.827919124216265}}, (5, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 34, 'savings': 14967.378430457662, 'criteria': 33.75054369282571}}, (5, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 35, 'savings': 19682.90393058517, 'criteria': 34.67195160115691}}, (5, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 36, 'savings': 24473.426447453785, 'criteria': 35.592109559209106}}, (5, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 37, 'savings': 29341.020189966523, 'criteria': 36.51098299683389}}, (5, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 38, 'savings': 34287.85233212996, 'criteria': 37.4285357944645}}, (5, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 39, 'savings': 39316.17765922098, 'criteria': 38.34473037234632}}, (5, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 40, 'savings': 44428.35176718708, 'criteria': 39.259527470546885}}, (5, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 41, 'savings': 49626.832970368836, 'criteria': 40.172886117160516}}, (6, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 30, 'savings': 3767.451540814538, 'criteria': 29.937209140986425}}, (6, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 31, 'savings': 8354.679610717445, 'criteria': 30.860755339821377}}, (6, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 32, 'savings': 13013.14141183658, 'criteria': 31.783114309802723}}, (6, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 33, 'savings': 17744.574924670058, 'criteria': 32.70425708458883}}, (6, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 34, 'savings': 22550.777835938476, 'criteria': 33.62415370273436}}, (6, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 35, 'savings': 27304.823188530594, 'criteria': 34.54491961352449}}, (6, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 36, 'savings': 32134.109740891112, 'criteria': 35.46443150431848}}, (6, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 37, 'savings': 37040.70278302384, 'criteria': 36.38265495361627}}, (6, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 38, 'savings': 42026.753550128444, 'criteria': 37.29955410749786}}, (6, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 39, 'savings': 47094.50539528468, 'criteria': 38.21509157674526}}, (6, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 40, 'savings': 52246.29661820402, 'criteria': 39.1292283896966}}, (6, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 41, 'savings': 57484.56769619511, 'criteria': 40.041923871730084}}, (7, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 30, 'savings': 10793.287902759475, 'criteria': 29.82011186828734}}, (7, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 31, 'savings': 15572.328486820166, 'criteria': 30.74046119188633}}, (7, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 32, 'savings': 20262.582413101365, 'criteria': 31.662290293114978}}, (7, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 33, 'savings': 25025.53855220516, 'criteria': 32.58290769079658}}, (7, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 34, 'savings': 29862.96794346036, 'criteria': 33.502283867608995}}, (7, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 35, 'savings': 34776.703124853, 'criteria': 34.42038828125245}}, (7, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 36, 'savings': 39642.269009913914, 'criteria': 35.33929551650144}}, (7, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 37, 'savings': 44585.31555120053, 'criteria': 36.25691140747999}}, (7, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 38, 'savings': 49607.97934135409, 'criteria': 37.173200344310764}}, (7, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 39, 'savings': 54712.4871761937, 'criteria': 38.0881252137301}}, (7, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 40, 'savings': 59901.15986972504, 'criteria': 39.001647335504586}}, (7, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 40, 'savings': 5133.635677922466, 'criteria': 39.91443940536796}}, (8, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 30, 'savings': 17528.683467539755, 'criteria': 29.707855275541004}}, (8, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 31, 'savings': 22496.574998338023, 'criteria': 30.625057083361032}}, (8, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 32, 'savings': 27377.206279906186, 'criteria': 31.54371322866823}}, (8, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 33, 'savings': 32170.485215484878, 'criteria': 32.46382524640858}}, (8, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 34, 'savings': 37037.93567445656, 'criteria': 33.38270107209239}}, (8, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 35, 'savings': 41981.36096225226, 'criteria': 34.30031065062913}}, (8, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 36, 'savings': 47002.62841376573, 'criteria': 35.216622859770574}}, (8, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 37, 'savings': 51979.71522857663, 'criteria': 36.133671412857055}}, (8, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 38, 'savings': 57036.521739866206, 'criteria': 37.049391304335565}}, (8, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 38, 'savings': 2157.278797416344, 'criteria': 37.96404535337639}}, (8, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 39, 'savings': 7337.078097371392, 'criteria': 38.87771536504381}}, (8, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 40, 'savings': 12602.789697831766, 'criteria': 39.78995350503614}}, (9, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 30, 'savings': 23977.9468297274, 'criteria': 29.600367552837877}}, (9, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 31, 'savings': 29131.785030195926, 'criteria': 30.514470249496735}}, (9, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 32, 'savings': 34199.862093915726, 'criteria': 31.430002298434736}}, (9, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 33, 'savings': 39182.08298084557, 'criteria': 32.34696528365257}}, (9, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 34, 'savings': 44078.388017866324, 'criteria': 33.26536019970223}}, (9, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 35, 'savings': 49050.3328148249, 'criteria': 34.18249445308625}}, (9, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 36, 'savings': 54099.754949823764, 'criteria': 35.09833741750294}}, (9, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 37, 'savings': 59228.5537026799, 'criteria': 36.01285743828867}}, (9, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 37, 'savings': 4281.482469396906, 'criteria': 36.928641958843386}}, (9, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 38, 'savings': 9409.316994606714, 'criteria': 37.84317805008989}}, (9, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 39, 'savings': 14620.6948643816, 'criteria': 38.756321752260305}}, (9, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 40, 'savings': 19917.97104435748, 'criteria': 39.66803381592737}}, (10, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 30, 'savings': 30145.3038118029, 'criteria': 29.497578269803284}}, (10, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 31, 'savings': 35482.23763997838, 'criteria': 30.408629372667026}}, (10, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 32, 'savings': 40734.88690672665, 'criteria': 31.32108521822122}}, (10, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 33, 'savings': 45903.149189494026, 'criteria': 32.23494751350843}}, (10, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 34, 'savings': 50986.95969825037, 'criteria': 33.150217338362495}}, (10, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 35, 'savings': 55986.29082025085, 'criteria': 34.06689515299582}}, (10, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 35, 'savings': 1053.9470349547482, 'criteria': 34.98243421608409}}, (10, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 36, 'savings': 6166.758089353891, 'criteria': 35.89722069851077}}, (10, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 37, 'savings': 11359.813840088478, 'criteria': 36.81066976933186}}, (10, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 38, 'savings': 16516.965098242916, 'criteria': 37.724717248362616}}, (10, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 39, 'savings': 21757.619486401964, 'criteria': 38.63737300855997}}, (10, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 40, 'savings': 27084.10939003765, 'criteria': 39.548598176832705}}, (11, 0): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 30, 'savings': 36034.89483102936, 'criteria': 29.399418419482846}}, (11, 1): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 31, 'savings': 41552.12780499531, 'criteria': 30.30746453658341}}, (11, 2): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 32, 'savings': 46986.52850563883, 'criteria': 31.216891191572685}}, (11, 3): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 33, 'savings': 52337.98941032785, 'criteria': 32.127700176494535}}, (11, 4): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 34, 'savings': 57606.43628507072, 'criteria': 33.03989272858215}}, (11, 5): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 34, 'savings': 2768.763380795368, 'criteria': 33.953853943653414}}, (11, 6): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 35, 'savings': 7828.9523530243605, 'criteria': 34.86951746078293}}, (11, 7): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 36, 'savings': 12967.068144104902, 'criteria': 35.78388219759825}}, (11, 8): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 37, 'savings': 18184.998928923473, 'criteria': 36.696916684517944}}, (11, 9): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 38, 'savings': 23484.698403374627, 'criteria': 37.60858835994376}}, (11, 10): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 39, 'savings': 28752.4485574677, 'criteria': 38.52079252404221}}, (11, 11): {'credit': {'success': False, 'code': 'S_i > C, Not needed'}, 'savings': {'success': True, 'months': 40, 'savings': 34105.92276935378, 'criteria': 39.4315679538441}}}}}},
        "expanded": {
            83927978940983: {
                32342390239: {
                    'savings': [{'debts': {}, 'incomes': {'before_savings': 2000000, 'regular_income': 0, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0}, {'debts': {}, 'incomes': {'before_savings': 2000000, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 10000, 'after_savings': 2010000}, {'debts': {}, 'incomes': {'before_savings': 2010000, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 10000, 'after_savings': 2020000}, {'debts': {}, 'incomes': {'before_savings': 2020000, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 10000, 'after_savings': 2030000}, {'debts': {}, 'incomes': {'before_savings': 2030000, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 10000, 'after_savings': 2040000}, {'debts': {}, 'incomes': {'before_savings': 2040000, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 10000, 'after_savings': 2050000}, {'debts': {}, 'incomes': {'before_savings': 2050000, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 10000, 'after_savings': 2060000}, {'debts': {}, 'incomes': {'before_savings': 2060000, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 10000, 'after_savings': 2070000}, {'debts': {}, 'incomes': {'before_savings': 2070000, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 10000, 'after_savings': 2080000}, {'debts': {}, 'incomes': {'before_savings': 2080000, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 10000, 'after_savings': 2090000}, {'debts': {}, 'incomes': {'before_savings': 2090000, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 10000, 'after_savings': 2100000}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 0, 'before_minimal_payment': 0, 'before_closing_payment': 0, 'actual_payment': 0, 'after_debt': 736472.83, 'after_minimal_payment': 5490.943570847791, 'after_closing_payment': 740462.0578291666}}, 'incomes': {'before_savings': 2100000, 'regular_income': 60000, 'irregular_income': 736472.83}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'mortgage_expenses': 60000, 'buy_realty': 1800000}}, 'to_save': 0, 'last_save': 10000, 'after_savings': 986472.8300000001}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 736472.83, 'before_minimal_payment': 5490.943570847791, 'before_closing_payment': 740462.0578291666, 'actual_payment': 5490.9436, 'after_debt': 734971.1142291666, 'after_minimal_payment': 5490.9435706299955, 'after_closing_payment': 738952.2077645747}}, 'incomes': {'before_savings': 986472.8300000001, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 4509.0564, 'last_save': 0, 'after_savings': 990981.8864000001}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 734971.1142291666, 'before_minimal_payment': 5490.9435706299955, 'before_closing_payment': 738952.2077645747, 'actual_payment': 5490.9435706299955, 'after_debt': 733461.2641939446, 'after_minimal_payment': 5490.9435706299955, 'after_closing_payment': 737434.1793749952}}, 'incomes': {'before_savings': 990981.8864000001, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 4509.0564293700045, 'last_save': 0, 'after_savings': 995490.94282937}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 733461.2641939446, 'before_minimal_payment': 5490.9435706299955, 'before_closing_payment': 737434.1793749952, 'actual_payment': 5490.9435706299955, 'after_debt': 731943.2358043652, 'after_minimal_payment': 5490.943570629996, 'after_closing_payment': 735907.9283316389}}, 'incomes': {'before_savings': 995490.94282937, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {'repairing': 1000000}}, 'to_save': 4509.0564293700045, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 731943.2358043652, 'before_minimal_payment': 5490.943570629996, 'before_closing_payment': 735907.9283316389, 'actual_payment': 10000.0, 'after_debt': 725907.928331639, 'after_minimal_payment': 5457.046529724577, 'after_closing_payment': 729839.9296101021}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 725907.928331639, 'before_minimal_payment': 5457.046529724577, 'before_closing_payment': 729839.9296101021, 'actual_payment': 10000.0, 'after_debt': 719839.9296101022, 'after_minimal_payment': 5422.822766230548, 'after_closing_payment': 723739.0625621569}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 719839.9296101022, 'before_minimal_payment': 5422.822766230548, 'before_closing_payment': 723739.0625621569, 'actual_payment': 10000.0, 'after_debt': 713739.062562157, 'after_minimal_payment': 5388.268040145825, 'after_closing_payment': 717605.1491510354}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 50000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 713739.062562157, 'before_minimal_payment': 5388.268040145825, 'before_closing_payment': 717605.1491510354, 'actual_payment': 60000.0, 'after_debt': 657605.1491510358, 'after_minimal_payment': 4975.103654773057, 'after_closing_payment': 661167.1770422705}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 657605.1491510358, 'before_minimal_payment': 4975.103654773057, 'before_closing_payment': 661167.1770422705, 'actual_payment': 60000.0, 'after_debt': 601167.177042271, 'after_minimal_payment': 4557.917019924777, 'after_closing_payment': 604423.49925125}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 601167.177042271, 'before_minimal_payment': 4557.917019924777, 'before_closing_payment': 604423.49925125, 'actual_payment': 60000.0, 'after_debt': 544423.4992512504, 'after_minimal_payment': 4136.655288763285, 'after_closing_payment': 547372.4598721947}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 544423.4992512504, 'before_minimal_payment': 4136.655288763285, 'before_closing_payment': 547372.4598721947, 'actual_payment': 60000.0, 'after_debt': 487372.45987219515, 'after_minimal_payment': 3711.264697057081, 'after_closing_payment': 490012.3940298362}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 487372.45987219515, 'before_minimal_payment': 3711.264697057081, 'before_closing_payment': 490012.3940298362, 'actual_payment': 60000.0, 'after_debt': 430012.39402983664, 'after_minimal_payment': 3281.6905431610744, 'after_closing_payment': 432341.62783083157}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.005416666666666667, 'before_debt': 430012.39402983664, 'before_minimal_payment': 3281.6905431610744, 'before_closing_payment': 432341.62783083157, 'actual_payment': 60000.0, 'after_debt': 372341.62783083203, 'after_minimal_payment': 3653.6354121105537, 'after_closing_payment': 375444.4747294223}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 372341.62783083203, 'before_minimal_payment': 3653.6354121105537, 'before_closing_payment': 375444.4747294223, 'actual_payment': 60000.0, 'after_debt': 315444.47472942254, 'after_minimal_payment': 3099.9125894072718, 'after_closing_payment': 318073.17868550104}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 315444.47472942254, 'before_minimal_payment': 3099.9125894072718, 'before_closing_payment': 318073.17868550104, 'actual_payment': 60000.0, 'after_debt': 258073.17868550125, 'after_minimal_payment': 2539.9117376249574, 'after_closing_payment': 260223.7885078804}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 258073.17868550125, 'before_minimal_payment': 2539.9117376249574, 'before_closing_payment': 260223.7885078804, 'actual_payment': 60000.0, 'after_debt': 200223.7885078806, 'after_minimal_payment': 1973.5450931464923, 'after_closing_payment': 201892.32007877962}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 200223.7885078806, 'before_minimal_payment': 1973.5450931464923, 'before_closing_payment': 201892.32007877962, 'actual_payment': 60000.0, 'after_debt': 141892.32007877983, 'after_minimal_payment': 1400.7233290958861, 'after_closing_payment': 143074.75607943634}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 141892.32007877983, 'before_minimal_payment': 1400.7233290958861, 'before_closing_payment': 143074.75607943634, 'actual_payment': 60000.0, 'after_debt': 83074.75607943651, 'after_minimal_payment': 821.3555204183133, 'after_closing_payment': 83767.04571343181}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 83074.75607943651, 'before_minimal_payment': 821.3555204183133, 'before_closing_payment': 83767.04571343181, 'actual_payment': 60000.0, 'after_debt': 23767.045713432024, 'after_minimal_payment': 235.34910801677879, 'after_closing_payment': 23965.104427710623}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0007412600098177791}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 23767.045713432024, 'before_minimal_payment': 235.34910801677879, 'before_closing_payment': 23965.104427710623, 'actual_payment': 23965.104427710627, 'after_debt': 0, 'after_minimal_payment': 0, 'after_closing_payment': 0}}, 'incomes': {'before_savings': -0.0007412600098177791, 'regular_income': 60000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 36034.89557228937, 'after_savings': 36034.89483102936}],
                }
            }
        }
    },
    "ex_2": {
        "input_data": {
            "personal_data": {
                "current_savings": 600000,
                "month_income": 100000,
                "month_rent": 20000,
                "month_expenses": 0,
                "deal_month_start": 9,
                "deal_month_finish": 30,
                "max_repairing_delay_months": 4,
                "inflation_percent": 0,
            },
            "credit_scheme": {
                "interest_rate": 15 / 1200,
                "months": 24,
            },
            "mortgage_schemes": {
                32342390239: {
                    "id": 32342390239,
                    "title": "12345678901234567890",
                    "initial_payment_percent": 10,
                    "initial_expenses": 60000,
                    "schedule": [
                        {
                            "interest_rate": 10 / 1200,
                            "months": 5,
                        },
                        {
                            "interest_rate": 15 / 1200,
                            "months": 48,
                        }
                    ]
                }
            },
            "realties": {
                83927978940983: {
                    "id": 83927978940983,
                    "title": "12345678901234567890",
                    "link": "https://yandex.ru",
                    "cost": 2700000,
                    "is_primary": True,
                    "get_keys_month": 13,
                    "repairing_expenses": 500000,
                    "repairing_months": 3,
                    "area": None,
                    "subway_distance": None,
                    "has_mall": None,
                }
            }
        },
        "output_data": {83927978940983: {'no_mortgage': {'opt': None, 'combs': {(9, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1320000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1320000, 'need': 2700000}}}, (9, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1320000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1320000, 'need': 2700000}}}, (9, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1320000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1320000, 'need': 2700000}}}, (9, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1320000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1320000, 'need': 2700000}}}, (10, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1400000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1400000, 'need': 2700000}}}, (10, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1400000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1400000, 'need': 2700000}}}, (10, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1400000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1400000, 'need': 2700000}}}, (10, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1400000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1400000, 'need': 2700000}}}, (11, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1480000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1480000, 'need': 2700000}}}, (11, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1480000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1480000, 'need': 2700000}}}, (11, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1480000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1480000, 'need': 2700000}}}, (11, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1480000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1480000, 'need': 2700000}}}, (12, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1560000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1560000, 'need': 2700000}}}, (12, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1560000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1560000, 'need': 2700000}}}, (12, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1560000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1560000, 'need': 2700000}}}, (12, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1560000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1560000, 'need': 2700000}}}, (13, 0): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1640000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1640000, 'need': 2700000}}}, (13, 1): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1640000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1640000, 'need': 2700000}}}, (13, 2): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1640000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1640000, 'need': 2700000}}}, (13, 3): {'savings': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1640000, 'need': 2700000}}, 'credit': {'success': False, 'code': 'S_i < C', 'payload': {'have': 1640000, 'need': 2700000}}}, (14,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (15,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (16,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (17,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (18,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (19,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (20,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (21,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (22,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (23,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (24,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (25,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (26,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (27,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (28,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}, (29,): {'savings': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}, 'credit': {'success': False, 'code': 'x_1 > g_k', 'message': 'Should be primary!'}}}}, 32342390239: {'opt': {'credit': {'case': 'S_i < C, S < S_t', 'x': (13, 0), 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 600000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 20000}, 'times': 13}, {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 1120000.0, 'schedule': [{'interest_rate': 0.008333333333333333, 'months': 5}, {'interest_rate': 0.0125, 'months': 48}], 'minimal_initial_sum': 270000.0, 'expenses': 60000}}, {'cmd': 'spend_savings', 'payload': {'buy_realty': 2700000}}, {'cmd': 'get_credit', 'payload': {'name': 'Repairing', 'requested_sum': 500000, 'schedule': [{'interest_rate': 0.0125, 'months': 24}], 'minimal_initial_sum': 0, 'expenses': 0}}, {'cmd': 'spend_savings', 'payload': {'repairing': 500000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 20000}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000}, 'times': 16}], 'result': {'months': 32, 'savings': 31918.46672870645, 'criteria': 31.680815332712935}}, 'savings': {'case': 'S_i < C, S < S_t', 'x': (13, 1), 'scenario': [{'cmd': 'initiate', 'payload': {'savings': 600000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 20000}, 'times': 13}, {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 1576927.8, 'schedule': [{'interest_rate': 0.008333333333333333, 'months': 5}, {'interest_rate': 0.0125, 'months': 48}], 'minimal_initial_sum': 270000.0, 'expenses': 60000}}, {'cmd': 'spend_savings', 'payload': {'buy_realty': 2700000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 20000, 'to_save': 43072.198}}, {'cmd': 'spend_savings', 'payload': {'repairing': 500000}}, {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_rent': 20000}, 'times': 3}, {'cmd': 'make_month', 'payload': {'earned': 100000}, 'times': 15}], 'result': {'months': 32, 'savings': 18461.392401842313, 'criteria': 31.81538607598158}}}, 'combs': {(9, 0): {'credit': {'success': True, 'months': 33, 'savings': 53348.623298027254, 'criteria': 32.46651376701973}, 'savings': {'success': True, 'months': 33, 'savings': 38566.061234610475, 'criteria': 32.61433938765389}}, (9, 1): {'credit': {'success': True, 'months': 33, 'savings': 36864.64241624405, 'criteria': 32.63135357583756}, 'savings': {'success': True, 'months': 33, 'savings': 16378.169209246626, 'criteria': 32.83621830790754}}, (9, 2): {'credit': {'success': True, 'months': 33, 'savings': 20584.167471273337, 'criteria': 32.79415832528726}, 'savings': {'success': True, 'months': 34, 'savings': 94884.16181614103, 'criteria': 33.05115838183859}}, (9, 3): {'credit': {'success': True, 'months': 33, 'savings': 4504.68604414165, 'criteria': 32.95495313955858}, 'savings': {'success': True, 'months': 34, 'savings': 74237.37623083015, 'criteria': 33.257626237691696}}, (10, 0): {'credit': {'success': True, 'months': 33, 'savings': 74962.92008171143, 'criteria': 32.25037079918289}, 'savings': {'success': True, 'months': 33, 'savings': 66451.48957975864, 'criteria': 32.33548510420241}}, (10, 1): {'credit': {'success': True, 'months': 33, 'savings': 58209.40697436223, 'criteria': 32.417905930256374}, 'savings': {'success': True, 'months': 33, 'savings': 43830.19916945736, 'criteria': 32.56169800830543}}, (10, 2): {'credit': {'success': True, 'months': 33, 'savings': 41928.93202939163, 'criteria': 32.58071067970609}, 'savings': {'success': True, 'months': 33, 'savings': 22034.511434398126, 'criteria': 32.77965488565602}}, (10, 3): {'credit': {'success': True, 'months': 33, 'savings': 25849.450602259836, 'criteria': 32.7415054939774}, 'savings': {'success': True, 'months': 33, 'savings': 1042.49176238972, 'criteria': 32.989575082376106}}, (11, 0): {'credit': {'success': True, 'months': 33, 'savings': 95302.60761078581, 'criteria': 32.04697392389214}, 'savings': {'success': True, 'months': 33, 'savings': 93051.86042074511, 'criteria': 32.06948139579255}}, (11, 1): {'credit': {'success': True, 'months': 33, 'savings': 78279.75036886327, 'criteria': 32.217202496311366}, 'savings': {'success': True, 'months': 33, 'savings': 69990.97400088294, 'criteria': 32.30009025999117}}, (11, 2): {'credit': {'success': True, 'months': 33, 'savings': 61722.713805878695, 'criteria': 32.38277286194121}, 'savings': {'success': True, 'months': 33, 'savings': 47739.151930661654, 'criteria': 32.522608480693386}}, (11, 3): {'credit': {'success': True, 'months': 33, 'savings': 45643.23237874639, 'criteria': 32.54356767621253}, 'savings': {'success': True, 'months': 33, 'savings': 26329.366014128944, 'criteria': 32.73670633985871}}, (12, 0): {'credit': {'success': True, 'months': 32, 'savings': 14230.135588631674, 'criteria': 31.857698644113682}, 'savings': {'success': True, 'months': 32, 'savings': 18202.321774360942, 'criteria': 31.81797678225639}}, (12, 1): {'credit': {'success': True, 'months': 33, 'savings': 97116.56149773269, 'criteria': 32.02883438502268}, 'savings': {'success': True, 'months': 33, 'savings': 94925.2344604826, 'criteria': 32.05074765539517}}, (12, 2): {'credit': {'success': True, 'months': 33, 'savings': 80283.93175697488, 'criteria': 32.19716068243025}, 'savings': {'success': True, 'months': 33, 'savings': 72213.60691982094, 'criteria': 32.27786393080179}}, (12, 3): {'credit': {'success': True, 'months': 33, 'savings': 63921.07396559348, 'criteria': 32.36078926034406}, 'savings': {'success': True, 'months': 33, 'savings': 50325.58133775377, 'criteria': 32.496744186622465}}, (13, 0): {'credit': {'success': True, 'months': 32, 'savings': 31918.46672870645, 'criteria': 31.680815332712935}, 'savings': {'success': False, 'code': 'INFEASIBLE'}}, (13, 1): {'credit': {'success': True, 'months': 32, 'savings': 14576.432672627838, 'criteria': 31.85423567327372}, 'savings': {'success': True, 'months': 32, 'savings': 18461.392401842313, 'criteria': 31.81538607598158}}, (13, 2): {'credit': {'success': True, 'months': 33, 'savings': 97651.87445535352, 'criteria': 32.02348125544646}, 'savings': {'success': True, 'months': 33, 'savings': 95518.84351653427, 'criteria': 32.04481156483466}}, (13, 3): {'credit': {'success': True, 'months': 33, 'savings': 81007.369792665, 'criteria': 32.18992630207335}, 'savings': {'success': True, 'months': 33, 'savings': 73151.36707121872, 'criteria': 32.26848632928781}}, (14,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (15,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (16,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (17,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (18,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (19,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (20,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (21,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (22,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (23,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (24,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (25,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (26,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (27,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (28,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}, (29,): {'savings': {'success': False, 'message': 'Should be primary!'}, 'credit': {'success': False, 'message': 'Should be primary!'}}}}}},
        "expanded": {
            83927978940983: {
                32342390239: {
                    'savings': [{'debts': {}, 'incomes': {'before_savings': 600000, 'regular_income': 0, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0}, {'debts': {}, 'incomes': {'before_savings': 600000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 680000}, {'debts': {}, 'incomes': {'before_savings': 680000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 760000}, {'debts': {}, 'incomes': {'before_savings': 760000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 840000}, {'debts': {}, 'incomes': {'before_savings': 840000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 920000}, {'debts': {}, 'incomes': {'before_savings': 920000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1000000}, {'debts': {}, 'incomes': {'before_savings': 1000000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1080000}, {'debts': {}, 'incomes': {'before_savings': 1080000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1160000}, {'debts': {}, 'incomes': {'before_savings': 1160000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1240000}, {'debts': {}, 'incomes': {'before_savings': 1240000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1320000}, {'debts': {}, 'incomes': {'before_savings': 1320000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1400000}, {'debts': {}, 'incomes': {'before_savings': 1400000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1480000}, {'debts': {}, 'incomes': {'before_savings': 1480000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1560000}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 0, 'before_minimal_payment': 0, 'before_closing_payment': 0, 'actual_payment': 0, 'after_debt': 1576927.8, 'after_minimal_payment': 36927.80223191199, 'after_closing_payment': 1590068.865}}, 'incomes': {'before_savings': 1560000, 'regular_income': 100000, 'irregular_income': 1576927.8}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {'mortgage_expenses': 60000, 'buy_realty': 2700000}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 456927.7999999998}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 1576927.8, 'before_minimal_payment': 36927.80223191199, 'before_closing_payment': 1590068.865, 'actual_payment': 36927.802, 'after_debt': 1553141.063, 'after_minimal_payment': 36927.80223742598, 'after_closing_payment': 1566083.9051916667}}, 'incomes': {'before_savings': 456927.7999999998, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {'repairing': 500000}}, 'to_save': 43072.198, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 1553141.063, 'before_minimal_payment': 36927.80223742598, 'before_closing_payment': 1566083.9051916667, 'actual_payment': 80000.0, 'after_debt': 1486083.905191667, 'after_minimal_payment': 35887.645776071404, 'after_closing_payment': 1498467.9377349308}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 1486083.905191667, 'before_minimal_payment': 35887.645776071404, 'before_closing_payment': 1498467.9377349308, 'actual_payment': 80000.0, 'after_debt': 1418467.937734931, 'after_minimal_payment': 34805.25149560652, 'after_closing_payment': 1430288.503882722}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 1418467.937734931, 'before_minimal_payment': 34805.25149560652, 'before_closing_payment': 1430288.503882722, 'actual_payment': 80000.0, 'after_debt': 1350288.5038827222, 'after_minimal_payment': 33678.03296017423, 'after_closing_payment': 1361540.908081745}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 1350288.5038827222, 'before_minimal_payment': 33678.03296017423, 'before_closing_payment': 1361540.908081745, 'actual_payment': 100000.0, 'after_debt': 1261540.9080817453, 'after_minimal_payment': 35109.62744068499, 'after_closing_payment': 1277310.169432767}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 1261540.9080817453, 'before_minimal_payment': 35109.62744068499, 'before_closing_payment': 1277310.169432767, 'actual_payment': 100000.0, 'after_debt': 1177310.1694327674, 'after_minimal_payment': 33275.56222494198, 'after_closing_payment': 1192026.546550677}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 1177310.1694327674, 'before_minimal_payment': 33275.56222494198, 'before_closing_payment': 1192026.546550677, 'actual_payment': 100000.0, 'after_debt': 1092026.5465506774, 'after_minimal_payment': 31359.453232464908, 'after_closing_payment': 1105676.8783825608}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 1092026.5465506774, 'before_minimal_payment': 31359.453232464908, 'before_closing_payment': 1105676.8783825608, 'actual_payment': 100000.0, 'after_debt': 1005676.8783825611, 'after_minimal_payment': 29355.82752016057, 'after_closing_payment': 1018247.8393623432}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 1005676.8783825611, 'before_minimal_payment': 29355.82752016057, 'before_closing_payment': 1018247.8393623432, 'actual_payment': 100000.0, 'after_debt': 918247.8393623434, 'after_minimal_payment': 27258.714672864568, 'after_closing_payment': 929725.9373543727}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 918247.8393623434, 'before_minimal_payment': 27258.714672864568, 'before_closing_payment': 929725.9373543727, 'actual_payment': 100000.0, 'after_debt': 829725.937354373, 'after_minimal_payment': 25061.588958117612, 'after_closing_payment': 840097.5115713027}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 829725.937354373, 'before_minimal_payment': 25061.588958117612, 'before_closing_payment': 840097.5115713027, 'actual_payment': 100000.0, 'after_debt': 740097.511571303, 'after_minimal_payment': 22757.303217331377, 'after_closing_payment': 749348.7304659443}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 740097.511571303, 'before_minimal_payment': 22757.303217331377, 'before_closing_payment': 749348.7304659443, 'actual_payment': 100000.0, 'after_debt': 649348.7304659446, 'after_minimal_payment': 20338.013082483558, 'after_closing_payment': 657465.589596769}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 649348.7304659446, 'before_minimal_payment': 20338.013082483558, 'before_closing_payment': 657465.589596769, 'actual_payment': 100000.0, 'after_debt': 557465.5895967693, 'after_minimal_payment': 17795.08982530957, 'after_closing_payment': 564433.9094667289}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 557465.5895967693, 'before_minimal_payment': 17795.08982530957, 'before_closing_payment': 564433.9094667289, 'actual_payment': 100000.0, 'after_debt': 464433.90946672927, 'after_minimal_payment': 15119.019798669853, 'after_closing_payment': 470239.33333506336}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 464433.90946672927, 'before_minimal_payment': 15119.019798669853, 'before_closing_payment': 470239.33333506336, 'actual_payment': 100000.0, 'after_debt': 370239.3333350637, 'after_minimal_payment': 12299.28800023016, 'after_closing_payment': 374867.325001752}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 370239.3333350637, 'before_minimal_payment': 12299.28800023016, 'before_closing_payment': 374867.325001752, 'actual_payment': 100000.0, 'after_debt': 274867.32500175235, 'after_minimal_payment': 9324.242754568093, 'after_closing_payment': 278303.16656427423}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 274867.32500175235, 'before_minimal_payment': 9324.242754568093, 'before_closing_payment': 278303.16656427423, 'actual_payment': 100000.0, 'after_debt': 178303.16656427458, 'after_minimal_payment': 6180.9378422886375, 'after_closing_payment': 180531.956146328}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 178303.16656427458, 'before_minimal_payment': 6180.9378422886375, 'before_closing_payment': 180531.956146328, 'actual_payment': 100000.0, 'after_debt': 80531.95614632838, 'after_minimal_payment': 2854.9475655505125, 'after_closing_payment': 81538.60559815748}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': -0.0020000002114102244}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 80531.95614632838, 'before_minimal_payment': 2854.9475655505125, 'before_closing_payment': 81538.60559815748, 'actual_payment': 81538.60559815748, 'after_debt': 0, 'after_minimal_payment': 0, 'after_closing_payment': 0}}, 'incomes': {'before_savings': -0.0020000002114102244, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 18461.394401842525, 'after_savings': 18461.392401842313}],
                    'credit': [{'debts': {}, 'incomes': {'before_savings': 600000, 'regular_income': 0, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0}, {'debts': {}, 'incomes': {'before_savings': 600000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 680000}, {'debts': {}, 'incomes': {'before_savings': 680000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 760000}, {'debts': {}, 'incomes': {'before_savings': 760000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 840000}, {'debts': {}, 'incomes': {'before_savings': 840000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 920000}, {'debts': {}, 'incomes': {'before_savings': 920000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1000000}, {'debts': {}, 'incomes': {'before_savings': 1000000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1080000}, {'debts': {}, 'incomes': {'before_savings': 1080000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1160000}, {'debts': {}, 'incomes': {'before_savings': 1160000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1240000}, {'debts': {}, 'incomes': {'before_savings': 1240000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1320000}, {'debts': {}, 'incomes': {'before_savings': 1320000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1400000}, {'debts': {}, 'incomes': {'before_savings': 1400000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1480000}, {'debts': {}, 'incomes': {'before_savings': 1480000, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 1560000}, {'debts': {'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 0, 'before_minimal_payment': 0, 'before_closing_payment': 0, 'actual_payment': 0, 'after_debt': 1120000.0, 'after_minimal_payment': 26227.6678106261, 'after_closing_payment': 1129333.3333333333}, 'Repairing': {'interest_rate': 0.0125, 'before_debt': 0, 'before_minimal_payment': 0, 'before_closing_payment': 0, 'actual_payment': 0, 'after_debt': 500000, 'after_minimal_payment': 24243.324023475572, 'after_closing_payment': 506250.0}}, 'incomes': {'before_savings': 1560000, 'regular_income': 100000, 'irregular_income': 500000}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {'mortgage_expenses': 0, 'buy_realty': 2700000, 'repairing': 500000}}, 'to_save': 0, 'last_save': 80000, 'after_savings': 0.0}, {'debts': {'Repairing': {'interest_rate': 0.0125, 'before_debt': 500000, 'before_minimal_payment': 24243.324023475572, 'before_closing_payment': 506250.0, 'actual_payment': 53772.3321893739, 'after_debt': 452477.66781062627, 'after_minimal_payment': 22758.113654537465, 'after_closing_payment': 458133.6386582591}, 'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 1120000.0, 'before_minimal_payment': 26227.6678106261, 'before_closing_payment': 1129333.3333333333, 'actual_payment': 26227.6678106261, 'after_debt': 1103105.6655227072, 'after_minimal_payment': 26227.6678106261, 'after_closing_payment': 1112298.2127353963}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Repairing': {'interest_rate': 0.0125, 'before_debt': 452477.66781062627, 'before_minimal_payment': 22758.113654537465, 'before_closing_payment': 458133.6386582591, 'actual_payment': 53772.332189373905, 'after_debt': 404361.30646888533, 'after_minimal_payment': 21136.92672558358, 'after_closing_payment': 409415.8227997464}, 'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 1103105.6655227072, 'before_minimal_payment': 26227.6678106261, 'before_closing_payment': 1112298.2127353963, 'actual_payment': 26227.6678106261, 'after_debt': 1086070.5449247702, 'after_minimal_payment': 26227.66781062609, 'after_closing_payment': 1095121.1327991434}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Repairing': {'interest_rate': 0.0125, 'before_debt': 404361.30646888533, 'before_minimal_payment': 21136.92672558358, 'before_closing_payment': 409415.8227997464, 'actual_payment': 53772.33218937391, 'after_debt': 355643.4906103726, 'after_minimal_payment': 19360.337318014834, 'after_closing_payment': 360089.0342430023}, 'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 1086070.5449247702, 'before_minimal_payment': 26227.66781062609, 'before_closing_payment': 1095121.1327991434, 'actual_payment': 26227.66781062609, 'after_debt': 1068893.464988517, 'after_minimal_payment': 26227.66781062609, 'after_closing_payment': 1077800.910530088}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 20000}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Repairing': {'interest_rate': 0.0125, 'before_debt': 355643.4906103726, 'before_minimal_payment': 19360.337318014834, 'before_closing_payment': 360089.0342430023, 'actual_payment': 73772.3321893739, 'after_debt': 286316.7020536286, 'after_minimal_payment': 16268.626568780472, 'after_closing_payment': 289895.660829299}, 'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 1068893.464988517, 'before_minimal_payment': 26227.66781062609, 'before_closing_payment': 1077800.910530088, 'actual_payment': 26227.66781062609, 'after_debt': 1051573.242719462, 'after_minimal_payment': 26227.667810626088, 'after_closing_payment': 1060336.3530754575}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Repairing': {'interest_rate': 0.0125, 'before_debt': 286316.7020536286, 'before_minimal_payment': 16268.626568780472, 'before_closing_payment': 289895.660829299, 'actual_payment': 73772.33218937392, 'after_debt': 216123.32863992528, 'after_minimal_payment': 12849.716169116435, 'after_closing_payment': 218824.87024792435}, 'Mortgage': {'interest_rate': 0.008333333333333333, 'before_debt': 1051573.242719462, 'before_minimal_payment': 26227.667810626088, 'before_closing_payment': 1060336.3530754575, 'actual_payment': 26227.667810626088, 'after_debt': 1034108.6852648315, 'after_minimal_payment': 28780.018499782313, 'after_closing_payment': 1047035.0438306419}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Repairing': {'interest_rate': 0.0125, 'before_debt': 216123.32863992528, 'before_minimal_payment': 12849.716169116435, 'before_closing_payment': 218824.87024792435, 'actual_payment': 71219.98150021769, 'after_debt': 147604.88874770692, 'after_minimal_payment': 9208.299583824344, 'after_closing_payment': 149449.94985705326}, 'Mortgage': {'interest_rate': 0.0125, 'before_debt': 1034108.6852648315, 'before_minimal_payment': 28780.018499782313, 'before_closing_payment': 1047035.0438306419, 'actual_payment': 28780.018499782313, 'after_debt': 1018255.0253308597, 'after_minimal_payment': 28780.018499782316, 'after_closing_payment': 1030983.2131474954}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Repairing': {'interest_rate': 0.0125, 'before_debt': 147604.88874770692, 'before_minimal_payment': 9208.299583824344, 'before_closing_payment': 149449.94985705326, 'actual_payment': 71219.98150021769, 'after_debt': 78229.96835683583, 'after_minimal_payment': 5136.59803388919, 'after_closing_payment': 79207.84296129628}, 'Mortgage': {'interest_rate': 0.0125, 'before_debt': 1018255.0253308597, 'before_minimal_payment': 28780.018499782316, 'before_closing_payment': 1030983.2131474954, 'actual_payment': 28780.018499782316, 'after_debt': 1002203.1946477132, 'after_minimal_payment': 28780.018499782313, 'after_closing_payment': 1014730.7345808096}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Repairing': {'interest_rate': 0.0125, 'before_debt': 78229.96835683583, 'before_minimal_payment': 5136.59803388919, 'before_closing_payment': 79207.84296129628, 'actual_payment': 71219.98150021769, 'after_debt': 7987.86146107885, 'after_minimal_payment': 553.9320085165085, 'after_closing_payment': 8087.709729342336}, 'Mortgage': {'interest_rate': 0.0125, 'before_debt': 1002203.1946477132, 'before_minimal_payment': 28780.018499782313, 'before_closing_payment': 1014730.7345808096, 'actual_payment': 28780.018499782313, 'after_debt': 985950.7160810274, 'after_minimal_payment': 28780.01849978232, 'after_closing_payment': 998275.1000320403}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Repairing': {'interest_rate': 0.0125, 'before_debt': 7987.86146107885, 'before_minimal_payment': 553.9320085165085, 'before_closing_payment': 8087.709729342336, 'actual_payment': 8087.709729342336, 'after_debt': 0, 'after_minimal_payment': 0, 'after_closing_payment': 0}, 'Mortgage': {'interest_rate': 0.0125, 'before_debt': 985950.7160810274, 'before_minimal_payment': 28780.01849978232, 'before_closing_payment': 998275.1000320403, 'actual_payment': 91912.29027065766, 'after_debt': 906362.8097613829, 'after_minimal_payment': 26905.900740848014, 'after_closing_payment': 917692.3448834001}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 906362.8097613829, 'before_minimal_payment': 26905.900740848014, 'before_closing_payment': 917692.3448834001, 'actual_payment': 100000.0, 'after_debt': 817692.3448834005, 'after_minimal_payment': 24698.118401612388, 'after_closing_payment': 827913.499194443}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 817692.3448834005, 'before_minimal_payment': 24698.118401612388, 'before_closing_payment': 827913.499194443, 'actual_payment': 100000.0, 'after_debt': 727913.4991944433, 'after_minimal_payment': 22382.656282665663, 'after_closing_payment': 737012.4179343738}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 727913.4991944433, 'before_minimal_payment': 22382.656282665663, 'before_closing_payment': 737012.4179343738, 'actual_payment': 100000.0, 'after_debt': 637012.4179343742, 'after_minimal_payment': 19951.631968784215, 'after_closing_payment': 644975.0731585539}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 637012.4179343742, 'before_minimal_payment': 19951.631968784215, 'before_closing_payment': 644975.0731585539, 'actual_payment': 100000.0, 'after_debt': 544975.0731585542, 'after_minimal_payment': 17396.374880153366, 'after_closing_payment': 551787.2615730361}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 544975.0731585542, 'before_minimal_payment': 17396.374880153366, 'before_closing_payment': 551787.2615730361, 'actual_payment': 100000.0, 'after_debt': 451787.26157303643, 'after_minimal_payment': 14707.325225999452, 'after_closing_payment': 457434.60234269936}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 451787.26157303643, 'before_minimal_payment': 14707.325225999452, 'before_closing_payment': 457434.60234269936, 'actual_payment': 100000.0, 'after_debt': 357434.6023426997, 'after_minimal_payment': 11873.917003524008, 'after_closing_payment': 361902.53487198346}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 357434.6023426997, 'before_minimal_payment': 11873.917003524008, 'before_closing_payment': 361902.53487198346, 'actual_payment': 100000.0, 'after_debt': 261902.5348719838, 'after_minimal_payment': 8884.442023683767, 'after_closing_payment': 265176.31655788363}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 261902.5348719838, 'before_minimal_payment': 8884.442023683767, 'before_closing_payment': 265176.31655788363, 'actual_payment': 100000.0, 'after_debt': 165176.31655788393, 'after_minimal_payment': 5725.891274591822, 'after_closing_payment': 167241.02051485746}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 165176.31655788393, 'before_minimal_payment': 5725.891274591822, 'before_closing_payment': 167241.02051485746, 'actual_payment': 100000.0, 'after_debt': 67241.02051485782, 'after_minimal_payment': 2383.769090064227, 'after_closing_payment': 68081.53327129355}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 0, 'after_savings': 0.0}, {'debts': {'Mortgage': {'interest_rate': 0.0125, 'before_debt': 67241.02051485782, 'before_minimal_payment': 2383.769090064227, 'before_closing_payment': 68081.53327129355, 'actual_payment': 68081.53327129355, 'after_debt': 0, 'after_minimal_payment': 0, 'after_closing_payment': 0}}, 'incomes': {'before_savings': 0.0, 'regular_income': 100000, 'irregular_income': 0}, 'expenses': {'common': {'month_expenses': 0, 'month_rent': 0}, 'extra': {}}, 'to_save': 0, 'last_save': 31918.46672870645, 'after_savings': 31918.46672870645}],
                }
            }
        }
    }
}