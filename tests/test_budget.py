import unittest
from budget import Budget
from test_scheduler_cases import cases
from copy import deepcopy

precision = 2


class TestCreateInstance(unittest.TestCase):

    def test(self):
        current_savings = 1000000
        bt = Budget(savings=current_savings)
        self.assertEqual(isinstance(bt, Budget), True, 'Should be instance of Budget')
        self.assertEqual(bt.savings, current_savings, 'Should be %f' % current_savings)
        self.assertEqual(bt.balance, 0, 'Should be %f' % 0)


class TestMakeMonth(unittest.TestCase):

    def test(self):
        current_savings = 1000000
        month_income = 100000
        month_rent = 20000
        expected = {
            'savings': 1560000,
            'balance': 0,
        }
        bt = Budget(savings=current_savings)
        for i in range(7):
            bt.make_month(earned=month_income, month_rent=month_rent)

        self.assertEqual(bt.savings, expected['savings'], 'Should be %f' % expected['savings'])
        self.assertEqual(bt.balance, expected['balance'], 'Should be %f' % expected['balance'])


class TestExecutor(unittest.TestCase):

    def test_case_1_83927978940983_32342390239_savings(self):
        scenario = deepcopy(cases['ex__CantStart__x_1__0']['output_data'][83927978940983][32342390239]['opt']['savings']['scenario'])
        expected = deepcopy(cases['ex__CantStart__x_1__0']['output_data'][83927978940983][32342390239]['opt']['savings']['result'])
        expanded = deepcopy(cases['ex__CantStart__x_1__0']['expanded'][83927978940983][32342390239]['savings'])
        result = Budget.execute(scenario)
        self.assertEqual(result.months,
                         expected['months'],
                         'Should be %f' % expected['months'])
        self.assertEqual(round(result.savings, precision),
                         round(expected['savings'], precision),
                         'Should be %f' % expected['savings'])
        self.assertEqual(result.history, expanded)

    def test_case_2_83927978940983_nomrg_savings(self):
        scenario = deepcopy(cases['ex__CantStart__x_1__0_2']['output_data'][83927978940983]['no_mortgage']['opt']['savings']['scenario'])
        expected = deepcopy(cases['ex__CantStart__x_1__0_2']['output_data'][83927978940983]['no_mortgage']['opt']['savings']['result'])
        # expanded = deepcopy(cases['ex__CantStart__x_1__0_2']['expanded'][83927978940983]['no_mortgage']['savings'])
        result = Budget.execute(scenario)
        self.assertEqual(result.months,
                         expected['months'],
                         'Should be %f' % expected['months'])
        self.assertEqual(round(result.savings, precision),
                         round(expected['savings'], precision),
                         'Should be %f' % expected['savings'])
        # self.assertEqual(result.history, expanded)

    def test_case_2_83927978940983_nomrg_credit(self):
        # self.maxDiff = None
        scenario = deepcopy(cases['ex__CantStart__x_1__0_2']['output_data'][83927978940983]['no_mortgage']['opt']['credit']['scenario'])
        expected = deepcopy(cases['ex__CantStart__x_1__0_2']['output_data'][83927978940983]['no_mortgage']['opt']['credit']['result'])
        # expanded = deepcopy(cases['ex__CantStart__x_1__0_2']['expanded'][83927978940983]['no_mortgage']['credit'])
        result = Budget.execute(scenario)
        self.assertEqual(result.months,
                         expected['months'],
                         'Should be %f' % expected['months'])
        self.assertEqual(round(result.savings, precision),
                         round(expected['savings'], precision),
                         'Should be %f' % expected['savings'])
        # self.assertEqual(result.history, expanded)

    def test_case_2_83927978940983_32342390239_savings(self):
        scenario = deepcopy(cases['ex__CantStart__x_1__0_2']['output_data'][83927978940983][32342390239]['opt']['savings']['scenario'])
        expected = deepcopy(cases['ex__CantStart__x_1__0_2']['output_data'][83927978940983][32342390239]['opt']['savings']['result'])
        # expanded = deepcopy(cases['ex__CantStart__x_1__0_2']['expanded'][83927978940983][32342390239]['savings'])
        result = Budget.execute(scenario)
        self.assertEqual(result.months,
                         expected['months'],
                         'Should be %f' % expected['months'])
        self.assertEqual(round(result.savings, precision),
                         round(expected['savings'], precision),
                         'Should be %f' % expected['savings'])
        # self.assertEqual(result.history, expanded)

    def test_case_3_83927978940983_nomrg_savings(self):
        scenario = deepcopy(cases['ex__SuperGood__x_1__0']['output_data'][83927978940983]['no_mortgage']['opt']['savings']['scenario'])
        expected = deepcopy(cases['ex__SuperGood__x_1__0']['output_data'][83927978940983]['no_mortgage']['opt']['savings']['result'])
        # expanded = deepcopy(cases['ex__SuperGood__x_1__0']['expanded'][83927978940983]['no_mortgage']['savings'])
        result = Budget.execute(scenario)
        self.assertEqual(result.months,
                         expected['months'],
                         'Should be %f' % expected['months'])
        self.assertEqual(round(result.savings, precision),
                         round(expected['savings'], precision),
                         'Should be %f' % expected['savings'])
        # self.assertEqual(result.history, expanded)

    def test_case_4_83927978940983_nomrg_savings(self):
        scenario = deepcopy(cases['ex_si_more_c_credit']['output_data'][83927978940983]['no_mortgage']['opt']['savings']['scenario'])
        expected = deepcopy(cases['ex_si_more_c_credit']['output_data'][83927978940983]['no_mortgage']['opt']['savings']['result'])
        # expanded = deepcopy(cases['ex_si_more_c_credit']['expanded'][83927978940983]['no_mortgage']['savings'])
        result = Budget.execute(scenario)
        self.assertEqual(result.months,
                         expected['months'],
                         'Should be %f' % expected['months'])
        self.assertEqual(round(result.savings, precision),
                         round(expected['savings'], precision),
                         'Should be %f' % expected['savings'])
        # self.assertEqual(result.history, expanded)

    def test_case_4_83927978940983_nomrg_credit(self):
        scenario = deepcopy(cases['ex_si_more_c_credit']['output_data'][83927978940983]['no_mortgage']['opt']['credit']['scenario'])
        expected = deepcopy(cases['ex_si_more_c_credit']['output_data'][83927978940983]['no_mortgage']['opt']['credit']['result'])
        # expanded = deepcopy(cases['ex_si_more_c_credit']['expanded'][83927978940983]['no_mortgage']['credit'])
        result = Budget.execute(scenario)
        self.assertEqual(result.months,
                         expected['months'],
                         'Should be %f' % expected['months'])
        self.assertEqual(round(result.savings, precision),
                         round(expected['savings'], precision),
                         'Should be %f' % expected['savings'])
        # self.assertEqual(result.history, expanded)

    def test_case_4_83927978940983_32342390239_savings(self):
        scenario = deepcopy(cases['ex_si_more_c_credit']['output_data'][83927978940983][32342390239]['opt']['savings']['scenario'])
        expected = deepcopy(cases['ex_si_more_c_credit']['output_data'][83927978940983][32342390239]['opt']['savings']['result'])
        # expanded = deepcopy(cases['ex_si_more_c_credit']['expanded'][83927978940983][32342390239]['savings'])
        result = Budget.execute(scenario)
        self.assertEqual(result.months,
                         expected['months'],
                         'Should be %f' % expected['months'])
        self.assertEqual(round(result.savings, precision),
                         round(expected['savings'], precision),
                         'Should be %f' % expected['savings'])
        # self.assertEqual(result.history, expanded)

    def test_case_5_83927978940983_nomrg_savings(self):
        scenario = deepcopy(cases['ex_si_more_c_credit_infeasible']['output_data'][83927978940983]['no_mortgage']['opt']['savings']['scenario'])
        expected = deepcopy(cases['ex_si_more_c_credit_infeasible']['output_data'][83927978940983]['no_mortgage']['opt']['savings']['result'])
        # expanded = deepcopy(cases['ex_si_more_c_credit_infeasible']['expanded'][83927978940983]['no_mortgage']['savings'])
        result = Budget.execute(scenario)
        self.assertEqual(result.months,
                         expected['months'],
                         'Should be %f' % expected['months'])
        self.assertEqual(round(result.savings, precision),
                         round(expected['savings'], precision),
                         'Should be %f' % expected['savings'])
        # self.assertEqual(result.history, expanded)

    def test_case_5_83927978940983_nomrg_credit(self):
        scenario = deepcopy(cases['ex_si_more_c_credit_infeasible']['output_data'][83927978940983]['no_mortgage']['opt']['credit']['scenario'])
        expected = deepcopy(cases['ex_si_more_c_credit_infeasible']['output_data'][83927978940983]['no_mortgage']['opt']['credit']['result'])
        # expanded = deepcopy(cases['ex_si_more_c_credit_infeasible']['expanded'][83927978940983]['no_mortgage']['credit'])
        result = Budget.execute(scenario)
        self.assertEqual(result.months,
                         expected['months'],
                         'Should be %f' % expected['months'])
        self.assertEqual(round(result.savings, precision),
                         round(expected['savings'], precision),
                         'Should be %f' % expected['savings'])
        # self.assertEqual(result.history, expanded)

    def test_case_5_83927978940983_32342390239_credit(self):
        scenario = deepcopy(cases['ex_si_more_c_credit_infeasible']['output_data'][83927978940983][32342390239]['opt']['savings']['scenario'])
        expected = deepcopy(cases['ex_si_more_c_credit_infeasible']['output_data'][83927978940983][32342390239]['opt']['savings']['result'])
        # expanded = deepcopy(cases['ex_si_more_c_credit_infeasible']['expanded'][83927978940983][32342390239]['savings'])
        result = Budget.execute(scenario)
        self.assertEqual(result.months,
                         expected['months'],
                         'Should be %f' % expected['months'])
        self.assertEqual(round(result.savings, precision),
                         round(expected['savings'], precision),
                         'Should be %f' % expected['savings'])
        # self.assertEqual(result.history, expanded)

    def test_case_6_83927978940983_32342390239_savings(self):
        scenario = deepcopy(cases['ex_si_more_c_no_credit']['output_data'][83927978940983][32342390239]['opt']['savings']['scenario'])
        expected = deepcopy(cases['ex_si_more_c_no_credit']['output_data'][83927978940983][32342390239]['opt']['savings']['result'])
        # expanded = deepcopy(cases['ex_si_more_c_no_credit']['expanded'][83927978940983][32342390239]['savings'])
        result = Budget.execute(scenario)
        self.assertEqual(result.months,
                         expected['months'],
                         'Should be %f' % expected['months'])
        self.assertEqual(round(result.savings, precision),
                         round(expected['savings'], precision),
                         'Should be %f' % expected['savings'])
        # self.assertEqual(result.history, expanded)

    def test_case_7_83927978940983_32342390239_savings(self):
        scenario = deepcopy(cases['ex_2']['output_data'][83927978940983][32342390239]['opt']['savings']['scenario'])
        expected = deepcopy(cases['ex_2']['output_data'][83927978940983][32342390239]['opt']['savings']['result'])
        # expanded = deepcopy(cases['ex_2']['expanded'][83927978940983][32342390239]['savings'])
        result = Budget.execute(scenario)
        self.assertEqual(result.months,
                         expected['months'],
                         'Should be %f' % expected['months'])
        self.assertEqual(round(result.savings, precision),
                         round(expected['savings'], precision),
                         'Should be %f' % expected['savings'])
        # self.assertEqual(result.history, expanded)

    def test_case_7_83927978940983_32342390239_credit(self):
        scenario = deepcopy(cases['ex_2']['output_data'][83927978940983][32342390239]['opt']['credit']['scenario'])
        expected = deepcopy(cases['ex_2']['output_data'][83927978940983][32342390239]['opt']['credit']['result'])
        # expanded = deepcopy(cases['ex_2']['expanded'][83927978940983][32342390239]['credit'])
        result = Budget.execute(scenario)
        self.assertEqual(result.months,
                         expected['months'],
                         'Should be %f' % expected['months'])
        self.assertEqual(round(result.savings, precision),
                         round(expected['savings'], precision),
                         'Should be %f' % expected['savings'])
        # self.assertEqual(result.history, expanded)


class TestHistory(unittest.TestCase):

    def test(self):
        scenario = [
            {'cmd': 'initiate', 'payload': {'savings': 300000}},
            {'cmd': 'add_savings', 'payload': {'savings': 100000}},
            {'cmd': 'make_month', 'payload': {'earned': 100000}},
            {'cmd': 'spend_savings', 'payload': {'buy_realty': 10000, 'repairing': 30000}},
        #     # {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 1509490.8900000001, 'schedule': [{'interest_rate': 0.005416666666666667, 'months': 12}, {'interest_rate': 0.008333333333333333, 'months': 228}], 'minimal_initial_sum': 270000.0, 'expencies': 60000}},
        #     # {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'buy_realty': 1800000}}},
        #     # {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 50000}, 'to_save': 38745.641}},
        #     # {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 50000}, 'to_save': 38745.641444407745}, 'times': 2},
        #     # {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 50000}, 'to_save': 38745.64144440774}, 'times': 2},
        #     # {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 50000}, 'to_save': 38745.641444407745}, 'times': 5},
        #     # {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 50000}, 'to_save': 38745.64144440774}, 'times': 2},
        #     # {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 50000}, 'to_save': 35561.413}},
        #     # {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'repairing': 1000000}}},
        #     # {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': {'rent': 50000}, 'to_save': None}, 'times': 3},
        #     # {'cmd': 'make_month', 'payload': {'earned': 100000, 'month_expencies': None, 'to_save': None}, 'times': 15}
        ]
        # expected = None
        result = Budget.execute(scenario)
        print(result.history)
        # self.assertEqual(result.months,
        #                  expected['months'],
        #                  'Should be %f' % expected['months'])
        # self.assertEqual(round(result.savings, precision),
        #                  round(expected['savings'], precision),
        #                  'Should be %f' % expected['savings'])
        # pass




        # from budget import Budget
        #
        # scenario = [
        #     {'cmd': 'initiate', 'payload': {'savings': 300000}},
        #     {'cmd': 'add_savings', 'payload': {'savings': 100000}},
        #     {'cmd': 'make_month',
        #      'payload': {'earned': 100000, 'month_expencies': {'rent': 25000, 'month_expenses': 10000},
        #                  'to_save': 20000}},
        #     {'cmd': 'get_credit', 'payload': {'name': 'Mortgage', 'requested_sum': 1500000,
        #                                       'schedule': [{'interest_rate': 0.005416666666666667, 'months': 12},
        #                                                    {'interest_rate': 0.008333333333333333, 'months': 228}],
        #                                       'minimal_initial_sum': 270000.0, 'expencies': 60000}},
        #     {'cmd': 'spend_savings', 'payload': {'extra_expencies': {'buy_realty': 10000, 'repairing': 30000}}},
        #     {'cmd': 'make_month', 'payload': {'earned': 100000}},
        # ]
        #
        # result = Budget.execute(scenario)
        # print(result.history)


# from budget import Budget
# from prettytable import PrettyTable
# from test_scheduler_cases import cases
#
# for case_name in cases.keys():
#
#     print('*' * 100)
#     print(case_name)
#     print('Input Data:', cases[case_name]['input_data'])


    # for rlt_id, realty in cases[case_name]['output_data'].items():
    #     print('REALTY:', rlt_id)
    #     for mrg_id, mrg in realty.items():
    #         print('MORTGAGE:', mrg_id)
    #         if mrg['opt'] is not None:
    #             if 'savings' in mrg['opt']:
    #                 print('Savings')
    #                 print(mrg['opt']['savings']['result'])
    #                 print(mrg['opt']['savings']['case'])
    #                 print(mrg['opt']['savings']['x'])
    #                 result = Budget.execute(mrg['opt']['savings']['scenario'])
    #                 Budget.print_history(result)
    #                 print(result.months, result.savings)
    #
    #             if 'credit' in mrg['opt']:
    #                 print('Credit')
    #                 print(mrg['opt']['credit']['result'])
    #                 print(mrg['opt']['credit']['case'])
    #                 print(mrg['opt']['credit']['x'])
    #                 result = Budget.execute(mrg['opt']['savings']['scenario'])
    #                 Budget.print_history(result)
    #                 print(result.months, result.savings)
    #
    #         else:
    #             print('No opt')
