import unittest
from test_scheduler_cases import cases
from scheduler import Scheduler


class TestScheduling(unittest.TestCase):

    def test__case_1(self):
        self.maxDiff = None
        sch = Scheduler(cases['ex__CantStart__x_1__0']['input_data'])
        self.assertEqual(sch.solution,
                         cases['ex__CantStart__x_1__0']['output_data'])

    def test__case_2(self):
        self.maxDiff = None
        sch = Scheduler(cases['ex__CantStart__x_1__0_2']['input_data'])
        self.assertEqual(sch.solution,
                         cases['ex__CantStart__x_1__0_2']['output_data'])

    def test__case_3(self):
        self.maxDiff = None
        sch = Scheduler(cases['ex__SuperGood__x_1__0']['input_data'])
        self.assertEqual(sch.solution,
                         cases['ex__SuperGood__x_1__0']['output_data'])

    def test__case_4(self):
        self.maxDiff = None
        sch = Scheduler(cases['ex_si_more_c_credit']['input_data'])
        self.assertEqual(sch.solution,
                         cases['ex_si_more_c_credit']['output_data'])

    def test__case_5(self):
        self.maxDiff = None
        sch = Scheduler(cases['ex_si_more_c_credit_infeasible']['input_data'])
        self.assertEqual(sch.solution,
                         cases['ex_si_more_c_credit_infeasible']['output_data'])

    def test__case_6(self):
        self.maxDiff = None
        sch = Scheduler(cases['ex_si_more_c_no_credit']['input_data'])
        self.assertEqual(sch.solution,
                         cases['ex_si_more_c_no_credit']['output_data'])

    def test__case_7(self):
        self.maxDiff = None
        sch = Scheduler(cases['ex_2']['input_data'])
        self.assertEqual(sch.solution,
                         cases['ex_2']['output_data'])